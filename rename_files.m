% 
%     This function renames .txt file so that they are compliant with one drive policy
% 
%     Copyright (C) 2021 Julian Sorensen  
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.
% 

function rename_files(dir_name)

file_struct = dir(fullfile(dir_name, '**/*.txt'));

for file_idx = 1:length(file_struct)
    file_name      = file_struct(file_idx).name; 
    copy_file_name = file_name;
    dir_name       = file_struct(file_idx).folder;
    change_name    = 0;

    [ file_name, change_name ] = fn_change_str(file_name, '"', '_', change_name);
    [ file_name, change_name ] = fn_change_str(file_name, ':', '_', change_name);
    [ file_name, change_name ] = fn_change_str(file_name, '*', '_', change_name);
    [ file_name, change_name ] = fn_change_str(file_name, '<', '_', change_name);
    [ file_name, change_name ] = fn_change_str(file_name, '>', '_', change_name);
    [ file_name, change_name ] = fn_change_str(file_name, '?', '_', change_name);
    [ file_name, change_name ] = fn_change_str(file_name, '/', '_', change_name);
    [ file_name, change_name ] = fn_change_str(file_name, '\', '_', change_name);
    [ file_name, change_name ] = fn_change_str(file_name, '|', '_', change_name);
    [ file_name, change_name ] = fn_change_str(file_name, '&', 'and', change_name);
    [ file_name, change_name ] = fn_change_str(file_name, '@', '_', change_name);
    if change_name
        [ copy_file_name ' ' fullfile(dir_name, file_name) ]
        movefile(fullfile(dir_name, copy_file_name), fullfile(dir_name, file_name))
    end
end


%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
function [ output_str, output_change ] = fn_change_str(input_str, old_char, new_char, input_change)

if length(strfind(input_str, old_char)) > 0
    output_str    = strrep(input_str, old_char, new_char);
    output_change = 1;
else
    output_str    = input_str;
    output_change = input_change;
end