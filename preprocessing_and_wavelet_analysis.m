% 
%     This file contains the functions required to preprocess data and
%     calculate wavelet quantities that required by the function 
%     analyse_wavelet_data.m.
% 
%     Copyright (C) 2021 Julian Sorensen  
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.
% 
 

%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 
%
% Description: Performs preprocessing of data and calculates wavelet quantities. 
%     Data is saved to .mat files for further processing by function 
%     analyse_wavelet_data.m.
% 
% Input Parameters:
%     1. dir_name:           Name of directory
%     2. file_name:          Name of file to analysis
%     3. recalculate_quant:  Indicates whether to recalculate quantities 
%
% Ouput Parameters:
%     1. analysis_parameters:  Contains all parameters for analysis
%     2. processed_data:       Contains processed data (as opposed to raw data)   
%     3. cross_wavelet_data:   Contains results of cross wavelet coherence function
%     4. burst_estimates:      Contains information regarding when activity has been detected
% 
function [ analysis_parameters, processed_data, cross_wavelet_data, burst_estimates ] = preprocessing_and_wavelet_analysis(dir_name, file_name, recalculate_quant)

analysis_parameters = fn_load_analysis_parameters(dir_name, file_name);

processed_data_file_name = [ 'processed_data_' file_name(1:end-4) '.mat' ];
processed_data_file_path = fullfile(dir_name, processed_data_file_name);
if recalculate_quant
    processed_data = fn_preprocess_data(analysis_parameters);
    save(processed_data_file_path, 'processed_data');
else
    load(processed_data_file_path);
end

cross_wavelet_file_name = [ 'cross_wavelet_' file_name(1:end-4) '.mat' ];
cross_wavelet_file_path = fullfile(dir_name, cross_wavelet_file_name);
if recalculate_quant
    cross_wavelet_data = fn_cross_wavelet_analysis(processed_data, analysis_parameters);
    save(cross_wavelet_file_path, 'cross_wavelet_data');
else
    load(cross_wavelet_file_path);
end
    
activity_bursts_file_name = [ 'activity_bursts_wavelet_' file_name(1:end-4) '.mat' ];
activity_bursts_file_path = fullfile(dir_name, activity_bursts_file_name);
if recalculate_quant
    burst_estimates = fn_activity_wavelet_est(analysis_parameters, cross_wavelet_data);
    save(activity_bursts_file_path, 'burst_estimates');
else
    load(activity_bursts_file_path);
end


%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Description:  Performs cross wavelet coherence calculation required for analysis
%
% Input Parameters:
%     1. processed_data:      Contains processed data
%     2. analysis_parameter:  Contains parameters for analysis
%
% Ouput Parameters:
%     1. cross_wavelet_data:  Contains results of cross wavelet coherence function
% 
function cross_wavelet_data = fn_cross_wavelet_analysis(processed_data, analysis_parameters)

% Getting processed data for cross wavelet analysis
median_data_1 = processed_data.median_data_1;
median_data_2 = processed_data.median_data_2;
median_data_3 = processed_data.median_data_3;
median_data_4 = processed_data.median_data_4;

% Getting parameters for frequency range to perform wavelet analysis
freq_low_freq  = analysis_parameters.freq_est.freq_low_freq;
freq_high_freq = analysis_parameters.freq_est.freq_high_freq;

% Some data was recorded two ways, resulting in four time series rather
% two. This chooses what data to analyse
if (strcmp(analysis_parameters.type_str, 'ATR') == 1) && (length(median_data_3) > 5)
    data_1 = median_data_3;
    data_2 = median_data_4;
else
    data_1 = median_data_1;
    data_2 = median_data_2;
end

% Calculating wavelet coherence
[ wcoh, wcs, F_wcoh, ~, cwt_1, cwt_2 ] = wcoherence(data_1, data_2, processed_data.fs, ...
    'FrequencyLimits', [ freq_low_freq freq_high_freq ], 'VoicesPerOctave', 32);

% Getting indices maximising wcoh, and also getting phase at
% these points
[ max_val_cross, max_idx_cross] = max(abs(wcoh), [], 1);
max_freq_cross                  = F_wcoh(max_idx_cross);
max_phase_cross                 = angle(wcs(max_idx_cross));

% Getting frequencies maximising cwt for two site
[ max_val_1, max_idx_1 ] = max(abs(cwt_1), [], 1);
[ max_val_2, max_idx_2 ] = max(abs(cwt_2), [], 1);
max_freq_1               = F_wcoh(max_idx_1);
max_freq_2               = F_wcoh(max_idx_2);

% Finding cwt at site 2 and wcoh values, for frequency maximising cwt at site 1 
cwt_2_at_1_max = max_val_2 * 0;
wcoh_at_1_max  = max_val_2 * 0;
for tmp_idx_1 = 1:length(cwt_1)
    cwt_2_at_1_max(tmp_idx_1) = abs(cwt_2(max_idx_1(tmp_idx_1), tmp_idx_1));
    wcoh_at_1_max(tmp_idx_1)  = wcoh(max_idx_1(tmp_idx_1), tmp_idx_1);
end

cross_wavelet_data.fs              = processed_data.fs;
cross_wavelet_data.max_freq_1      = max_freq_1;
cross_wavelet_data.max_freq_2      = max_freq_2;
cross_wavelet_data.max_val_1       = max_val_1;
cross_wavelet_data.max_val_2       = max_val_2;
cross_wavelet_data.max_freq_cross  = max_freq_cross;
cross_wavelet_data.max_val_cross   = max_val_cross;
cross_wavelet_data.max_phase_cross = max_phase_cross;
cross_wavelet_data.cwt_2_at_1_max  = cwt_2_at_1_max;
cross_wavelet_data.wcoh_at_1_max   = wcoh_at_1_max;


%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Description:  Finds activity burst at oral end and compares before and after burst to oral end
%
% Input Parameters:
%     1. analysis_parameter:  Contains parameters for processing data
%     2. cross_wavelet_data:  Contains results of cross wavelet coherence function
% 
% Output Parameters:
%     1. burst_estimates:  Contains information regarding when activity has been detected
% 
function burst_estimates = fn_activity_wavelet_est(analysis_parameters, cross_wavelet_data)

% Extracting wavelet related data
max_freq_1      = cross_wavelet_data.max_freq_1;
max_freq_2      = cross_wavelet_data.max_freq_2;
max_val_1       = cross_wavelet_data.max_val_1;
max_val_2       = cross_wavelet_data.max_val_2;
max_freq_cross  = cross_wavelet_data.max_freq_cross;
max_val_cross   = cross_wavelet_data.max_val_cross;
max_phase_cross = cross_wavelet_data.max_phase_cross;

activity_bursts    = analysis_parameters.activity_bursts;
burst_len_sec      = activity_bursts.burst_len_sec;
burst_mask_len_sec = activity_bursts.burst_mask_len_sec;
clip_perc          = activity_bursts.clip_perc;
burst_len_samples  = round(burst_len_sec * 1000);
if 1%length(max_val_1) > (1.3 * burst_len_samples)
    [ burst_indices_1, avg_power_1 ] = fn_get_burst_indices(max_val_1, burst_len_sec, burst_mask_len_sec, 0, clip_perc);
    [ burst_indices_2, avg_power_2 ] = fn_get_burst_indices(max_val_2, burst_len_sec, burst_mask_len_sec, 0, clip_perc);    
    burst_estimates.burst_indices_1  = burst_indices_1 + round(0.5 * burst_len_samples);
    burst_estimates.avg_power_1      = avg_power_1;
    burst_estimates.burst_indices_2  = burst_indices_2 + round(0.5 * burst_len_samples);
    burst_estimates.avg_power_2      = avg_power_2;
else
    burst_estimates.burst_indices_1 = [];
    burst_estimates.avg_power_1     = [];
    burst_estimates.burst_indices_2 = [];
    burst_estimates.avg_power_2     = [];
end


%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
function [ burst_indices, avg_power ] = fn_get_burst_indices(input_data, burst_len_sec, burst_mask_len_sec, take_hilbert, clip_perc)

% Getting average power
burst_len_samples = round(burst_len_sec * 1000);
avg_power         = fn_avg_power_est(input_data, burst_len_samples, take_hilbert, clip_perc);

% Finding difference in average power between windows separated by burst_len_samples
tmp_diff          = avg_power(1+burst_len_samples:end) - avg_power(1:end-burst_len_samples);
greater_zero_vals = tmp_diff(find(tmp_diff > 0));
if 1%length(greater_zero_vals) > (0.01 * length(tmp_diff))
    greater_zero_vals_sorted = sort(greater_zero_vals);
    threshold_diff           = greater_zero_vals_sorted(round(0.8 * end));
    
    % Finding points in which average power has increased by a significant amount
    burst_mask_len_samples = round(burst_mask_len_sec * 1000);
    current_mask           = tmp_diff > threshold_diff;
    burst_indices          = [];
    for idx = 1:10
        tmp_idx = find(current_mask, 1);
        if length(tmp_idx) > 0
            [ ~, max_pos ]     = max(tmp_diff(tmp_idx(1):min(end, tmp_idx(1) + round(burst_len_samples * 0.7))));
            burst_indices(idx) = max_pos(1) + tmp_idx(1);
            current_mask(tmp_idx(1):min(end, tmp_idx(1) + burst_mask_len_samples)) = 0;
        end
    end
else
    burst_indices = [];
end


%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Description:  Calculate average power in a sliding window
%
% Input Parameters:
%     1. input_data:    Data over which window will be slid
%     2. win_len:       Length of window
%     3. take_hilbert:  Boolean variable indicating whether to take hilbert transform of data 
%     4. clip_perc:     Data is clipped to handle outliers. Variable is used to determine amount of clipping
%
% Ouput Parameters:
%     1. RMS_power:  Contains root mean square estimate of power
% 
function RMS_power = fn_avg_power_est(input_data, win_len, take_hilbert, clip_perc)

% Taking hilbert transform of data if required
if take_hilbert
    hilbert_data = hilbert(input_data);
else
    hilbert_data = input_data;
end

% Clipping data
len_data        = length(hilbert_data);
abs_data        = abs(hilbert_data);
sort_abs_data   = sort(abs_data);
data_threshold  = sort_abs_data(round(end * clip_perc / 100));
below_threshold = (abs_data < data_threshold);
above_threshold = (abs_data > data_threshold);

% Calculating RMS power
clipped_data = (abs_data .* below_threshold) + (above_threshold * data_threshold); 
avg_power    = conv(clipped_data.^2, ones(win_len, 1), 'same') / win_len;
RMS_power    = sqrt(avg_power);


%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Description:  Applies median filter and FIR filter to data
%
% Input Parameters:
%     1. analysis_parameter:  Contains parameters for processing data
%
% Ouput Parameters:
%     1. processed_data:  Contains processed data
% 
function processed_data = fn_preprocess_data(analysis_parameters)

data_mat = fn_load_raw_data(analysis_parameters);
fs       = 1 / (data_mat(2,1) - data_mat(1,1));
if size(data_mat, 2) == 5
    median_data_1   = fn_apply_median_filter(data_mat(:, 2), analysis_parameters);
    median_data_2   = fn_apply_median_filter(data_mat(:, 4), analysis_parameters);
    median_data_3   = fn_apply_median_filter(data_mat(:, 3), analysis_parameters);
    median_data_4   = fn_apply_median_filter(data_mat(:, 5), analysis_parameters);
    low_pass_data_1 = fn_apply_low_pass_data(median_data_1, analysis_parameters, fs);
    low_pass_data_2 = fn_apply_low_pass_data(median_data_2, analysis_parameters, fs);
    low_pass_data_3 = fn_apply_low_pass_data(median_data_3, analysis_parameters, fs);
    low_pass_data_4 = fn_apply_low_pass_data(median_data_4, analysis_parameters, fs);
else
    median_data_1   = fn_apply_median_filter(data_mat(:, 2), analysis_parameters);
    median_data_2   = fn_apply_median_filter(data_mat(:, 3), analysis_parameters);
    low_pass_data_1 = fn_apply_low_pass_data(median_data_1, analysis_parameters, fs);
    low_pass_data_2 = fn_apply_low_pass_data(median_data_2, analysis_parameters, fs);
    median_data_3 = []; low_pass_data_3 = [];
    median_data_4 = []; low_pass_data_4 = [];
end
processed_data.fs              = fs;
processed_data.median_data_1   = median_data_1;
processed_data.median_data_2   = median_data_2;
processed_data.median_data_3   = median_data_3;
processed_data.median_data_4   = median_data_4;
processed_data.low_pass_data_1 = low_pass_data_1;
processed_data.low_pass_data_2 = low_pass_data_2;
processed_data.low_pass_data_3 = low_pass_data_3;
processed_data.low_pass_data_4 = low_pass_data_4;


%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Description:  Applies low pass filter to data
% 
% Input Parameters:
%     1. input_data:           Data to be filtered
%     2. analysis_parameters:  Contains parameters for filter 
%     3. fs:                   Sample rate
%
% Ouput Parameters:
%     1. output_data:  Filtered data
% 
function output_data = fn_apply_low_pass_data(input_data, analysis_parameters, fs)

lowpass_filters = analysis_parameters.lowpass_filters;
filt_len        = lowpass_filters.filt_len;
LF_Hz           = lowpass_filters.LF_Hz; 
HF_Hz           = lowpass_filters.HF_Hz;
filt_coef       = fir1(filt_len, [ LF_Hz, HF_Hz ] / fs);
output_data     = filter(filt_coef, 1, input_data);
output_data     = output_data(filt_len:end);


%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Description:  Loads raw data

% Input Parameters:
%     1. analysis_parameters:  Contains name of file. 
%
% Ouput Parameters:
%     1. data_mat:  Matrix whose columns contain raw data. Note that first
%                   column is time stamps. 
% 
function data_mat = fn_load_raw_data(analysis_parameters)

dir_name  = analysis_parameters.dir_name;
file_name = analysis_parameters.file_name;
file_path = fullfile(dir_name, file_name);
full_str  = fileread(file_path);
part_str  = full_str(1:400);
if (isempty(strfind(part_str, 'Interval=')) || isempty(strfind(part_str, 'ms')) || isempty(strfind(part_str, 'BottomValue='))) == 0
    tmp_idx = strfind(part_str, 'Range=') + length('Range=');
    CR_idx  = strfind(part_str(tmp_idx:tmp_idx+100), sprintf('\r'));
    tmp_str = part_str(tmp_idx:CR_idx(1) + tmp_idx - 2);
    if length(strfind(tmp_str, 'V')) == 2 
        no_columns = 3;
    else
        no_columns = 5;
    end
    tmp_idx  = strfind(part_str, 'BottomValue=') + length('BottomValue=');
    CR_idx   = strfind(part_str(tmp_idx:tmp_idx+100), sprintf('\r'));
    data_str = full_str(CR_idx(1) + tmp_idx:end);
    data_vec = sscanf(data_str, '%f');
else
    CR_idx     = strfind(part_str(1:100), sprintf('\r'));
    no_columns = length(sscanf(part_str(1:CR_idx(1)-2), '%f'));
    data_vec   = sscanf(full_str, '%f');
end
num_rows = length(data_vec) / no_columns;
data_mat = reshape(data_vec, no_columns, num_rows)';


%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Description:  Removes slow moving trend by applying median filter
%
% Input Parameters:
%     1. input_data:           Vector to which median filter will be applied
%     2. analysis_parameters:  Contains parameters for analysis
%                           
% Ouput Parameters:
%     1. output_data:  Filtered data
% 
function output_data = fn_apply_median_filter(input_data, analysis_parameters)

remove_amt      = 20;
median_filt_len = analysis_parameters.median_filt_len;
median_trend    = medfilt1(input_data, median_filt_len);
median_data     = input_data(remove_amt:end-remove_amt) - median_trend(remove_amt:end-remove_amt);
output_data     = median_data; 
% len_input_data     = length(input_data);
% len_median_section = len_input_data + 1 - median_filt_len;
% middle_offset      = round(median_filt_len / 2);
% output_data        = zeros(len_input_data, 1);
% 
% output_data(1:middle_offset) = input_data(1:middle_offset) - median(input_data(1:median_filt_len));
% for idx = 1:len_median_section
%     output_data(idx + middle_offset) = input_data(idx + middle_offset) - median(input_data(idx:idx+median_filt_len-1));
% end
% output_data(end-middle_offset:end) = input_data(end-middle_offset:end) - median(input_data(end+1-median_filt_len:end));


% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Description:  Clips large values that can occour due measurements
%
% Input Parameters:
%     1. input_data:  Contains data that may require clipping
%                           
% Ouput Parameters:
%     1. output_data:  Data that has been clipped
% 
function output_data = fn_clip_data(input_data)

perc_val    = prctile(abs(input_data), 99.9);
clip_val    = 20 * perc_val;
clip_data   = (input_data > clip_val);
keep_data   = 1 - clip_data; 
output_data = (input_data .* keep_data) + (sign(input_data) .* clip_data * clip_val);


%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Description:  Gets parameters for analysis
%
% Input Parameters:
%     1. dir_name:   Name of directory
%     2. file_name:  Name of file to analysis
%
% Ouput Parameters:
%     1. analysis_parameters:  Contains all parameters for analysis
% 
function analysis_parameters = fn_load_analysis_parameters(dir_name, file_name)

[ dist_mm, dist_idx, type_str, type_idx ] = get_distance_type(file_name);

analysis_parameters.dir_name  = dir_name;
analysis_parameters.file_name = file_name;
analysis_parameters.dist_mm   = dist_mm;
analysis_parameters.dist_idx  = dist_idx;
analysis_parameters.type_str  = type_str;
analysis_parameters.type_idx  = type_idx;

if strcmp(type_str, 'NIC')
    lowpass_filters.filt_len           = 1000;
    lowpass_filters.LF_Hz              = 0.5;
    lowpass_filters.HF_Hz              = 3.5;
    freq_est.freq_low_freq             = 1;
    freq_est.freq_high_freq            = 2.8;
    activity_bursts.burst_len_sec      = 15;
    activity_bursts.burst_mask_len_sec = 90;
    activity_bursts.clip_perc          = 95;
    recognised_type                    = 1;
end

if strcmp(type_str, 'KRB')
    lowpass_filters.filt_len           = 1000;
    lowpass_filters.LF_Hz              = 0.5;
    lowpass_filters.HF_Hz              = 3.5;
    freq_est.freq_low_freq             = 1;
    freq_est.freq_high_freq            = 2.8;
    activity_bursts.burst_len_sec      = 15;
    activity_bursts.burst_mask_len_sec = 90;
    activity_bursts.clip_perc          = 95;
    recognised_type                    = 1;
end     

if strcmp(type_str, 'ROD')
    lowpass_filters.filt_len           = 1000;
    lowpass_filters.LF_Hz              = 0.5;
    lowpass_filters.HF_Hz              = 3.5;
    freq_est.freq_low_freq             = 1;
    freq_est.freq_high_freq            = 2.8;
    activity_bursts.burst_len_sec      = 15;
    activity_bursts.burst_mask_len_sec = 90;
    activity_bursts.clip_perc          = 95;
    recognised_type                    = 1;
end

if strcmp(type_str, 'FLD')
    lowpass_filters.filt_len           = 1000;
    lowpass_filters.LF_Hz              = 0.5;
    lowpass_filters.HF_Hz              = 3.5;
    freq_est.freq_low_freq             = 1;
    freq_est.freq_high_freq            = 2.8;
    activity_bursts.burst_len_sec      = 15;
    activity_bursts.burst_mask_len_sec = 90;
    activity_bursts.clip_perc          = 95;
    recognised_type                    = 1;
end

if strcmp(type_str, 'PUMP OFF')
    lowpass_filters.filt_len           = 1000;
    lowpass_filters.LF_Hz              = 0.1;
    lowpass_filters.HF_Hz              = 3.5;
    freq_est.freq_low_freq             = 1;
    freq_est.freq_high_freq            = 2.8;
    activity_bursts.burst_len_sec      = 15;
    activity_bursts.burst_mask_len_sec = 90;
    activity_bursts.clip_perc          = 95;
    recognised_type                    = 1;
end

if strcmp(type_str, 'PUMP ON')
    lowpass_filters.filt_len           = 1000;
    lowpass_filters.LF_Hz              = 0.1;
    lowpass_filters.HF_Hz              = 3.5;
    freq_est.freq_low_freq             = 1;
    freq_est.freq_high_freq            = 2.8;
    activity_bursts.burst_len_sec      = 15;
    activity_bursts.burst_mask_len_sec = 90;
    activity_bursts.clip_perc          = 95;
    recognised_type                    = 1;
end

if strcmp(type_str, 'ATR')
    lowpass_filters.filt_len           = 1000;
    lowpass_filters.LF_Hz              = 0.5;
    lowpass_filters.HF_Hz              = 2;
    freq_est.freq_low_freq             = 0.5;
    freq_est.freq_high_freq            = 1.5;
    activity_bursts.burst_len_sec      = 15;
    activity_bursts.burst_mask_len_sec = 90;
    activity_bursts.clip_perc          = 95;
    recognised_type                    = 1;
end

if strcmp(type_str, 'TTX')
    lowpass_filters.filt_len           = 1000;
    lowpass_filters.LF_Hz              = 0.25;
    lowpass_filters.HF_Hz              = 3.5;
    freq_est.freq_low_freq             = 0.5;
    freq_est.freq_high_freq            = 2.5;
    activity_bursts.burst_len_sec      = 15;
    activity_bursts.burst_mask_len_sec = 30;
    activity_bursts.clip_perc          = 95;
    recognised_type                    = 1;
end

if recognised_type == 0
    lowpass_filters.filt_len           = 1000;
    lowpass_filters.LF_Hz              = 0.5;
    lowpass_filters.HF_Hz              = 3.5;
    freq_est.freq_low_freq             = 1;
    freq_est.freq_high_freq            = 2.8;
    activity_bursts.burst_len_sec      = 15;
    activity_bursts.burst_mask_len_sec = 90;
    activity_bursts.clip_perc          = 95;
    recognised_type                    = 0;
end


analysis_parameters.median_filt_len = 2000;
analysis_parameters.lowpass_filters = lowpass_filters;
analysis_parameters.freq_est        = freq_est;
analysis_parameters.activity_bursts = activity_bursts;
analysis_parameters.recognised_type = recognised_type;