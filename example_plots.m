% 
%     This file generates figure 2 and figure 9 in paper.
% 
%     Copyright (C) 2021 Julian Sorensen  
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.
% 


function example_plots

clc; clear all; close all

% example_file = 'Fluid 1mm';
example_file = 'Fluid 30mm';
% example_file = 'TTX 2mm';
% example_file = 'TTX 30mm';
switch example_file
    case 'Fluid 1mm'
        example_dir_name  = 'FLUID DISTENTION/20180912 FLUID/';
        example_file_name = '20180912 1mm FLUID1.txt'; LHS_time = 14; RHS_time = 40; 
    case 'Fluid 30mm'
        example_dir_name  = 'FLUID DISTENTION/20180912 FLUID/';
        example_file_name = '20180912 30mm FLUID4.txt'; LHS_time = 10; RHS_time = 40;     
    case 'TTX 2mm'
        example_dir_name  = 'TTX DATA ROD';
        example_file_name = '20210413 2mm TTX.txt'; LHS_time = 0; RHS_time = 200;        
    case 'TTX 30mm'
        example_dir_name  = 'TTX DATA ROD';
        example_file_name = '20210413 30mm TTX.txt'; LHS_time = 0; RHS_time = 200;
    otherwise
        return
end
dir_name = fullfile(pwd, '/data/synchronised_electrode_recordings');
example_dir_name = fullfile(dir_name, example_dir_name);
[ analysis_parameters, processed_data, ~, ~ ] = preprocessing_and_wavelet_analysis(example_dir_name, example_file_name, 0);

use_time_bnds       = 1;
fontsize_title      = 20;
fontsize_axis_label = 18;
fontsize_axis_no    = 16;
title_len           = 45;
titles_visible      = 1;
font_name_label     = 'arial';
continue_lettering  = 1;

fs              = processed_data.fs;
low_pass_data_1 = processed_data.low_pass_data_1;
low_pass_data_2 = processed_data.low_pass_data_2;
low_pass_data_3 = processed_data.low_pass_data_3;
low_pass_data_4 = processed_data.low_pass_data_4;
median_data_1   = processed_data.median_data_1;
median_data_2   = processed_data.median_data_2;
median_data_3   = processed_data.median_data_3;
median_data_4   = processed_data.median_data_4;


%% Plotting raw data (with median filter applied to remove unstable baseline)
figure();
subplot(3,4,1);
x_axis = [ 0:length(median_data_1)-1 ] / fs;
plot(x_axis, median_data_1 / max(abs(median_data_1)), 'r'); axis tight; hold on;
plot(x_axis, median_data_2 / max(abs(median_data_2)), 'b'); axis tight;
set(gca, 'LineWidth', 1);
set(gca, 'fontsize', fontsize_axis_no);
xlabel('time (sec)', 'fontsize', fontsize_axis_label, 'fontname', font_name_label);
if titles_visible
    if continue_lettering == 0
        t = title(fn_make_title('A', 'Raw Data'), 'fontsize', fontsize_title, 'fontname', font_name_label);
    else
        t = title(fn_make_title('L', 'Raw Data'), 'fontsize', fontsize_title, 'fontname', font_name_label);
    end
    set(t, 'horizontalAlignment', 'left'); set(t, 'units', 'normalized'); h1 = get(t, 'position'); set(t, 'position', [0 h1(2) h1(3)]);
end
if use_time_bnds; xlim([ LHS_time, RHS_time ]); end


%% Plotting low pass filtered data with position of troughs overlayed
subplot(3,4,[5 6]);
x_axis         = [ 0:length(low_pass_data_1)-1 ] / fs;
low_peak_pos_1 = fn_find_peaks(low_pass_data_1, fs);
low_peak_pos_2 = fn_find_peaks(low_pass_data_2, fs);
scaled_data_1  = low_pass_data_1 / max(abs(low_pass_data_1));
scaled_data_2  = low_pass_data_2 / max(abs(low_pass_data_2));
plot(x_axis, low_pass_data_1 / max(abs(low_pass_data_1)), 'r'); axis tight; hold on;
plot(x_axis, low_pass_data_2 / max(abs(low_pass_data_2)), 'b'); axis tight;
low_filt_min_1 = -1;
low_filt_max_1 = 1.2;
y_vals         = [ low_filt_min_1 - 0.1, low_filt_max_1 ];
for idx = 1:length(low_peak_pos_1)
    x_vals = [ x_axis(low_peak_pos_1(idx)), x_axis(low_peak_pos_1(idx)) ];
    y_vals = [ scaled_data_1(low_peak_pos_1(idx))+0.1, low_filt_max_1 ];
    plot(x_vals, y_vals, 'Color', 'r');
end
y_vals = [ low_filt_min_1, low_filt_max_1 + 0.1 ];
for idx = 1:length(low_peak_pos_2)
    x_vals = [ x_axis(low_peak_pos_2(idx)), x_axis(low_peak_pos_2(idx)) ];
    y_vals = [ scaled_data_2(low_peak_pos_2(idx))+0.1, low_filt_max_1 ];
    plot(x_vals, y_vals,  'Color', 'b');
end
set(gca, 'LineWidth', 1);
set(gca, 'fontsize', fontsize_axis_no);
xlabel('time (sec)', 'fontsize', fontsize_axis_label, 'fontname', font_name_label);
if titles_visible
    if continue_lettering == 0
        t = title(fn_make_title('E', 'Filtered Data'), 'fontsize', fontsize_title, 'fontname', font_name_label);
    else
        t = title(fn_make_title('P', 'Filtered Data'), 'fontsize', fontsize_title, 'fontname', font_name_label);
    end
    set(t, 'horizontalAlignment', 'left'); set(t, 'units', 'normalized'); h1 = get(t, 'position'); set(t, 'position', [0 h1(2) h1(3)]);
end
if use_time_bnds; xlim([ LHS_time, RHS_time ]); end


%% Plotting log-likehood surfaces for frequency estimates
subplot(3,4,7); AX = newplot;
log_likelihood_results = fn_log_like_frequency_est(processed_data, fs, analysis_parameters);
log_like_freq_1        = log_likelihood_results.log_like_freq_1;
log_like_freq_2        = log_likelihood_results.log_like_freq_2;
max_freq_1             = log_likelihood_results.max_freq_1;
max_freq_2             = log_likelihood_results.max_freq_2;
x_axis                 = [ 0:(size(log_like_freq_1, 2) - 1) ] / 50;
y_axis                 = log_likelihood_results.selected_freqs;
imagesc(x_axis, y_axis, log_like_freq_1);
AX.YDir = 'normal'; set(gca, 'LineWidth', 1); set(gca, 'fontsize', fontsize_axis_no);
xlabel('time (sec)', 'fontsize', fontsize_axis_label, 'fontname', font_name_label);
ylabel('frequency (Hz)', 'fontsize', fontsize_axis_label, 'fontname', font_name_label);
if titles_visible
    if continue_lettering == 0
        t = title(fn_make_title('F', 'Log Likelihood (Proximal)'), 'fontsize', ...
            fontsize_title, 'fontname', font_name_label);
    else
        t = title(fn_make_title('Q', 'Log Likelihood (Proximal)'), 'fontsize', ...
            fontsize_title, 'fontname', font_name_label);
    end
    set(t, 'horizontalAlignment', 'left'); set(t, 'units', 'normalized');
    h1 = get(t, 'position'); set(t, 'position', [0 h1(2) h1(3)]);
end
if use_time_bnds; xlim([ LHS_time, RHS_time ]); end

subplot(3,4,8); AX = newplot;
imagesc(x_axis, y_axis, log_like_freq_2);
AX.YDir = 'normal'; set(gca, 'LineWidth', 1);
set(gca, 'fontsize', fontsize_axis_no);
xlabel('time (sec)', 'fontsize', fontsize_axis_label, 'fontname', font_name_label);
ylabel('frequency (Hz)', 'fontsize', fontsize_axis_label, 'fontname', font_name_label);
if titles_visible
    if continue_lettering == 0
        t = title(fn_make_title('G', 'Log Likelihood (Distal)'), 'fontsize', ...
            fontsize_title, 'fontname', font_name_label);
    else
        t = title(fn_make_title('R', 'Log Likelihood (Distal)'), 'fontsize', ...
            fontsize_title, 'fontname', font_name_label);
    end
    set(t, 'horizontalAlignment', 'left'); set(t, 'units', 'normalized');
    h1 = get(t, 'position'); set(t, 'position', [0 h1(2) h1(3)]);
end
if use_time_bnds; xlim([ LHS_time, RHS_time ]); end

subplot(3,4,9);
x_axis = [ 0:length(max_freq_1)-1 ] / 1000;
plot(x_axis, max_freq_1); axis tight; hold on; plot(x_axis, max_freq_2);
max_val = 1.5 * max(max(max_freq_1), max(max_freq_2));
min_val = min(min(max_freq_1), min(max_freq_2));
set(gca, 'LineWidth', 1); set(gca, 'fontsize', fontsize_axis_no);
xlabel('time (sec)', 'fontsize', fontsize_axis_label, 'fontname', font_name_label);
ylabel('frequency (Hz)', 'fontsize', fontsize_axis_label, 'fontname', font_name_label);
if titles_visible
    if continue_lettering == 0
        t = title(fn_make_title('H', 'ML Frequency Estimates'), 'fontsize', ...
            fontsize_title, 'fontname', font_name_label);
    else
        t = title(fn_make_title('S', 'ML Frequency Estimates'), 'fontsize', ...
            fontsize_title, 'fontname', font_name_label);
    end
    set(t, 'horizontalAlignment', 'left'); set(t, 'units', 'normalized'); h1 = get(t, 'position'); set(t, 'position', [0 h1(2) h1(3)]);
end
if use_time_bnds; xlim([ LHS_time, RHS_time ]); end


%% Plotting cross correlation and auto correlation
cross_correlation_param.corr_len = 3000;
cross_correlation_param.step_len = round(3000 / 32);
cross_correlation_param.p_start  = 300;
cross_correlation_param.p_end    = 1800;
lowpass_filters.LF_Hz            = 0.5;
lowpass_filters.HF_Hz            = 10;
lowpass_filters.filt_len         = 1000;
filt_data_1                      = fn_apply_low_pass_data(median_data_1, lowpass_filters, fs);
filt_data_2                      = fn_apply_low_pass_data(median_data_2, lowpass_filters, fs);

subplot(3,4,10); AX = newplot;
[ xcorr_buffer, time_lag_pnts, corr_time_pnts ] = ...
    fn_sliding_cross_corr(filt_data_1, filt_data_2, cross_correlation_param, fs);
imagesc(corr_time_pnts, time_lag_pnts, (xcorr_buffer)'); axis tight;
AX.YDir = 'normal'; set(gca, 'LineWidth', 1); set(gca, 'fontsize', fontsize_axis_no);
xlabel('time (sec)', 'fontsize', fontsize_axis_label, 'fontname', font_name_label);
ylabel('time lag (sec)', 'fontsize', fontsize_axis_label, 'fontname', font_name_label);
if titles_visible
    if continue_lettering == 0
        t = title(fn_make_title('I', 'Cross Correlation'), 'fontsize', ...
            fontsize_title, 'fontname', font_name_label);
    else
        t = title(fn_make_title('T', 'Cross Correlation'), 'fontsize', ...
            fontsize_title, 'fontname', font_name_label);
    end
    set(t, 'horizontalAlignment', 'left'); set(t, 'units', 'normalized'); h1 = get(t, 'position'); set(t, 'position', [0 h1(2) h1(3)]);
end
if use_time_bnds; xlim([ LHS_time, RHS_time ]); end

subplot(3,4,11); AX = newplot;
[ xcorr_buffer, time_lag_pnts, corr_time_pnts ] = ...
    fn_sliding_cross_corr(filt_data_1, filt_data_1, cross_correlation_param, fs);
tmp_num = round(size(xcorr_buffer, 2) / 2);
imagesc(corr_time_pnts, time_lag_pnts(tmp_num:end), (xcorr_buffer(:,tmp_num:end))'); axis tight;
AX.YDir = 'normal'; set(gca, 'LineWidth', 1); set(gca, 'fontsize', fontsize_axis_no);
xlabel('time (sec)', 'fontsize', fontsize_axis_label, 'fontname', font_name_label);
ylabel('time lag (sec)', 'fontsize', fontsize_axis_label, 'fontname', font_name_label);
if titles_visible
    if continue_lettering == 0
        t = title(fn_make_title('J', 'Auto Correlation (Proximal)'), 'fontsize', ...
            fontsize_title, 'fontname', font_name_label);
    else
        t = title(fn_make_title('U', 'Auto Correlation (Proximal)'), 'fontsize', ...
            fontsize_title, 'fontname', font_name_label);
    end
    set(t, 'horizontalAlignment', 'left'); set(t, 'units', 'normalized'); h1 = get(t, 'position'); set(t, 'position', [0 h1(2) h1(3)]);
end
if use_time_bnds; xlim([ LHS_time, RHS_time ]); end

subplot(3,4,12); AX = newplot;
[ xcorr_buffer, time_lag_pnts, corr_time_pnts ] = ...
    fn_sliding_cross_corr(filt_data_2, filt_data_2, cross_correlation_param, fs);
tmp_num = round(size(xcorr_buffer, 2) / 2);
imagesc(corr_time_pnts, time_lag_pnts(tmp_num:end), (xcorr_buffer(:,tmp_num:end))'); axis tight;
AX.YDir = 'normal'; set(gca, 'fontsize', fontsize_axis_no);
xlabel('time (sec)', 'fontsize', fontsize_axis_label, 'fontname', font_name_label);
ylabel('time lag (sec)', 'fontsize', fontsize_axis_label, 'fontname', font_name_label);
if titles_visible
    if continue_lettering == 0
        t = title(fn_make_title('K', 'Auto Correlation (Distal)'), 'fontsize', ...
            fontsize_title, 'fontname', font_name_label);
    else
        t = title(fn_make_title('V', 'Auto Correlation (Distal)'), 'fontsize', ...
            fontsize_title, 'fontname', font_name_label);
    end
    set(t, 'horizontalAlignment', 'left'); set(t, 'units', 'normalized'); h1 = get(t, 'position'); set(t, 'position', [0 h1(2) h1(3)]);
end
if use_time_bnds; xlim([ LHS_time, RHS_time ]); end


%% Plotting wavelet coherence and continuous wavelet transforms
[ wcoh, ~, freq_vals, ~, cwt_1, cwt_2 ] = wcoherence(processed_data.median_data_1, processed_data.median_data_2, 1 / fs);
wcoh  = downsample(wcoh.', 16).';
cwt_1 = downsample(cwt_1.', 16).';
cwt_2 = downsample(cwt_2.', 16).';

fn_plot_wavelet_data(abs(wcoh), freq_vals, 3, 4, 2, fs);
set(gca, 'LineWidth', 1); set(gca, 'fontsize', fontsize_axis_no);
xlabel('time (sec)', 'fontsize', fontsize_axis_label, 'fontname', font_name_label);
ylabel('freq (Hz)', 'fontsize', fontsize_axis_label, 'fontname', font_name_label);
if titles_visible
    if continue_lettering == 0
        t = title(fn_make_title('B', 'Wavelet Coherence'), 'fontsize', ...
            fontsize_title, 'fontname', font_name_label);
    else
        t = title(fn_make_title('M', 'Wavelet Coherence'), 'fontsize', ...
            fontsize_title, 'fontname', font_name_label);
    end
    set(t, 'horizontalAlignment', 'left'); set(t, 'units', 'normalized'); h1 = get(t, 'position'); set(t, 'position', [0 h1(2) h1(3)]);
end
if use_time_bnds; xlim([ LHS_time, RHS_time ]); end

fn_plot_wavelet_data(abs(cwt_1), freq_vals, 3, 4, 3, fs);
set(gca, 'LineWidth', 1); set(gca, 'fontsize', fontsize_axis_no);
xlabel('time (sec)', 'fontsize', fontsize_axis_label, 'fontname', font_name_label);
ylabel('freq (Hz)', 'fontsize', fontsize_axis_label, 'fontname', font_name_label);
if titles_visible
    if continue_lettering == 0
        t = title(fn_make_title('C', 'CWT (Proximal)'), 'fontsize', ...
            fontsize_title, 'fontname', font_name_label);
    else
        t = title(fn_make_title('N', 'CWT (Proximal)'), 'fontsize', ...
            fontsize_title, 'fontname', font_name_label);
    end
    set(t, 'horizontalAlignment', 'left'); set(t, 'units', 'normalized'); h1 = get(t, 'position'); set(t, 'position', [0 h1(2) h1(3)]);
end
if use_time_bnds; xlim([ LHS_time, RHS_time ]); end

fn_plot_wavelet_data(abs(cwt_2), freq_vals, 3, 4, 4, fs);
set(gca, 'LineWidth', 1); set(gca, 'fontsize', fontsize_axis_no);
xlabel('time (sec)', 'fontsize', fontsize_axis_label, 'fontname', font_name_label);
ylabel('freq (Hz)', 'fontsize', fontsize_axis_label, 'fontname', font_name_label);
if titles_visible
    if continue_lettering == 0
        t = title(fn_make_title('D', 'CWT (Distal)'), 'fontsize', ...
            fontsize_title, 'fontname', font_name_label);
    else
        t = title(fn_make_title('O', 'CWT (Distal)'), 'fontsize', ...
            fontsize_title, 'fontname', font_name_label);
    end
    set(t, 'horizontalAlignment', 'left'); set(t, 'units', 'normalized'); h1 = get(t, 'position'); set(t, 'position', [0 h1(2) h1(3)]);
end
if use_time_bnds; xlim([ LHS_time, RHS_time ]); end




%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Description:  Calculates cross correlation between two time series. A window is slide over
%               the data, with the cross correlation calculated within each window 
%
% Input Parameters:
%     1. input_data_1:       Time series 1
%     2. input_data_2:       Time series 2
%     3. cross_correlation:  Parameters required for cross correlation parameters
%     4. fs:                 Sample rate
%
% Ouput Parameters:
%     1. xcorr_buffer:    Matrix containing cross correlation calculate over sliding window
%     2. time_lag_pnts:   The time lags considered
%     3. corr_time_pnts:  The time points for at which which cross correlation is calculated
%
function [ xcorr_buffer, time_lag_pnts, corr_time_pnts ] = ...
    fn_sliding_cross_corr(input_data_1, input_data_2, cross_correlation, fs)

corr_len     = cross_correlation.corr_len;
step_len     = cross_correlation.step_len;
no_samples   = length(input_data_1);
no_leave     = round(corr_len / 9);
st_pnt       = 1;
end_pnt      = st_pnt + corr_len - 1;
no_corr      = floor((no_samples - (1*corr_len)) / step_len);
xcorr_buffer = zeros(no_corr, (2 * corr_len) - 1 - (2*no_leave));
zero_vec     = zeros(1, (2 * corr_len) - 1);
scl_factor   = 1 ./ [ [ 1:1:(corr_len-1) ], [ corr_len:-1:1 ] ];
for corr_idx = 1:no_corr
    tmp_data_1 = input_data_1(st_pnt:end_pnt, :);
    tmp_data_2 = input_data_2(st_pnt:end_pnt, :);
    exp_data_1 = zero_vec;
    exp_data_2 = zero_vec;
    exp_data_1(corr_len+1:2*corr_len) = tmp_data_1;
    exp_data_2(corr_len+1:2*corr_len) = tmp_data_2;
    tmp_corr = xcorr(exp_data_1, exp_data_2) / (std(exp_data_1) * std(exp_data_2));
    tmp_corr = scl_factor .* tmp_corr(corr_len+1:corr_len + (2 * corr_len) - 1);
    xcorr_buffer(corr_idx+round(corr_len/(2*step_len)), :) = tmp_corr(no_leave+1:end-no_leave);
    st_pnt  = st_pnt + step_len;
    end_pnt = st_pnt + corr_len - 1;
end
time_lag_pnts  = [ -(corr_len-no_leave-1):(corr_len-no_leave-1) ] / fs;
corr_time_pnts = [ 0:(size(xcorr_buffer,1)-1) ] * step_len / fs;


%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Description:  Applies low pass filter to data
% 
% Input Parameters:
%     1. input_data:       Data to be filtered
%     2. lowpass_filters:  Contains parameters for filter 
%     3. fs:               Sample rate
%
% Ouput Parameters:
%     1. output_data:  Filtered data
% 
function output_data = fn_apply_low_pass_data(input_data, lowpass_filters, fs)

filt_len    = lowpass_filters.filt_len;
LF_Hz       = lowpass_filters.LF_Hz;
HF_Hz       = lowpass_filters.HF_Hz;
filt_coef   = fir1(filt_len, [ LF_Hz, HF_Hz ] / fs);
output_data = filter(filt_coef, 1, input_data);
output_data = output_data(filt_len:end);


%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Description:  Find peaks!
%
% Input Parameters:
%     1. input_data:  Time series for which function finds peaks
%     2. fs:          Sample rate
%
% Ouput Parameters:
%     1. peak_pos:  Position of detected peaks
%
function peak_pos = fn_find_peaks(input_data, fs)

trough_width_sec     = 0.1;
no_pnts_trough_width = round(trough_width_sec * fs);
no_pnts_left         = length(input_data) - (2 * no_pnts_trough_width) - 1;
peak_cond            = (ones(no_pnts_left + 1, 1) == 1);
for idx = 1:(no_pnts_trough_width - 1)
    peak_cond = (peak_cond .* (input_data(idx:(idx + no_pnts_left)) < input_data((idx+1):(idx + no_pnts_left + 1))));
end
for idx = no_pnts_trough_width:(2*no_pnts_trough_width)
    peak_cond = (peak_cond .* (input_data(idx:(idx + no_pnts_left)) > input_data((idx+1):(idx + no_pnts_left + 1))));
end
peak_pos = find(peak_cond) + no_pnts_trough_width - 1;


%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Description:  Creates titles for plots
%
% Input Parameters:
%     1. first_letter:  Letter proceeding main text for title
%     2. main_part:     Main text for title
%
% Ouput Parameters:
%     1. full_title:  Full title for plot
% 
function full_title = fn_make_title(first_letter, main_part)

full_title = [ first_letter, '.  ', main_part ];


%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Description:  Calculates log likelihood at each frequency and some other
%               parameters based upon it
%
% Input Parameters:
%     1. input_data:  Time series for which function finds peaks
%     2. fs:          Sample rate
%
% Ouput Parameters:
%     1. log_likelihood_results:  Contains log-likelihood results and other
%                                 parameters based upon it
%
function log_likelihood_results = fn_log_like_frequency_est(processed_data, fs, analysis_parameters)

type_str = analysis_parameters.type_str;
if (length(processed_data.low_pass_data_3) > 5) % && (strcmp(type_str, 'ATR') == 1)
    data_1 = processed_data.low_pass_data_3;
    data_2 = processed_data.low_pass_data_4;
else
    data_1 = processed_data.low_pass_data_1;
    data_2 = processed_data.low_pass_data_2;
end

[ max_val_1, max_freq_1, max_phase_1, log_like_freq_1, selected_freqs ] = fn_fine_log_like_freq(data_1, fs, analysis_parameters.freq_est);
[ max_val_2, max_freq_2, max_phase_2, log_like_freq_2, ~ ]              = fn_fine_log_like_freq(data_2, fs, analysis_parameters.freq_est);
log_likelihood_results.max_val_1       = max_val_1;
log_likelihood_results.max_freq_1      = max_freq_1;
log_likelihood_results.max_phase_1     = max_phase_1;
log_likelihood_results.log_like_freq_1 = log_like_freq_1;
log_likelihood_results.selected_freqs  = selected_freqs;
log_likelihood_results.max_val_2       = max_val_2;
log_likelihood_results.max_freq_2      = max_freq_2;
log_likelihood_results.max_phase_2     = max_phase_2;
log_likelihood_results.log_like_freq_2 = log_like_freq_2;


%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Description:  Calculates log-likelihood over a fine frequency grid over a sliding window.
%               Also tracks the frequency at which the likelihood is maximized over sliding window.
%
% Input Parameters:
%     1. input_data:  Time series containing for which log-likelihood is calculated
%     2. fs:          Sample rate
%     3. freq_est:    Contains frequency bounds over which to calculate log-likelihood
%
% Ouput Parameters:
%     1. max_val:         Vector containing maximum value of log-likehood over sliding window
%     2. max_freq:        Vector containing freq at which log-likehood is maximized over sliding window
%     3. max_phase:       Vector containing the phase at which at log-likehood is maximized over sliding window
%     4. log_like_freq:   Matrix containing log-likehood at each frequency over sliding window
%     5. selected_freqs:  Vector containing the frequencies for which log-likehood is calculated
%
function [ max_val, max_freq, max_phase, log_like_freq, selected_freqs ] = fn_fine_log_like_freq(input_data, fs, freq_est)

win_len        = 3000;
freq_res_Hz    = 0.005;
freq_low_freq  = freq_est.freq_low_freq;
freq_high_freq = freq_est.freq_high_freq;
selected_freqs = linspace(freq_low_freq, freq_high_freq, 1 + ceil((freq_high_freq - freq_low_freq) / freq_res_Hz));

input_data     = hilbert(input_data);
time_pnts      = [ 0:(win_len - 1) ] / fs;
len_data       = length(input_data);
log_like_freq  = [];
for freq_idx = 1:length(selected_freqs)
    current_freq        = selected_freqs(freq_idx);
    mixer_seq           = exp(sqrt(-1) * 2 * pi * time_pnts * selected_freqs(freq_idx));
    tmp_log_like_freq       = xcorr(input_data, mixer_seq);
    tmp_log_like_freq       = tmp_log_like_freq(len_data+1:end);
    tmp_abs_log_like_freq   = abs(tmp_log_like_freq);
    tmp_angle_log_like_freq = angle(tmp_log_like_freq);
    if freq_idx == 1
        max_val   = tmp_abs_log_like_freq;
        max_freq  = (max_val * 0) + current_freq;
        max_phase = tmp_angle_log_like_freq;
    else
        tmp_cond_1 = (max_val > tmp_abs_log_like_freq);
        tmp_cond_2 = 1 - tmp_cond_1;
        max_val    = (max_val   .* tmp_cond_1) + (tmp_abs_log_like_freq   .* tmp_cond_2);
        max_freq   = (max_freq  .* tmp_cond_1) + (current_freq        .* tmp_cond_2);
        max_phase  = (max_phase .* tmp_cond_1) + (tmp_angle_log_like_freq .* tmp_cond_2);
    end
    log_like_freq(freq_idx, :) = downsample(tmp_abs_log_like_freq, 20);
end


%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Description:  Plots wavelet data
%
% Input Parameters:
%     1. input_data:  Contains wavelet data
%     2. freq_vals:   Contains frequency values at which wavelet data was calcuated
%     3. no_rows:     Number of rows in figure
%     4. no_cols:     Number of columns in figure
%     5. plot_no:     Subplot number
%     6. fs:          Sample rate
%
function fn_plot_wavelet_data(input_data, freq_vals, no_rows, no_cols, plot_no, fs)

subplot(no_rows, no_cols, plot_no); AX = newplot;

freq_vals  = freq_vals * 1e6;
tmpIdx     = find(freq_vals < 0.5); tmpIdx = tmpIdx(1);
freq_vals  = freq_vals(1:tmpIdx);
input_data = input_data(1:tmpIdx, :);
x_axis     = [0:size(input_data,2)-1] * 16 / fs;
setappdata(AX,'evstruct',[]); cla(AX,'reset');
imagesc(x_axis, log2(freq_vals), abs(input_data)); axis tight;

Yticks   = 2.^(round(log2(min(freq_vals))):round(log2(max(freq_vals))));
AX.YLim  = log2([min(freq_vals), max(freq_vals)]);
AX.YTick = log2(Yticks);
AX.YDir  = 'normal';
set(AX,'YLim',log2([min(freq_vals),max(freq_vals)]), ...
    'layer','top', ...
    'YTick',log2(Yticks(:)), ...
    'YTickLabel',num2str(sprintf('%g\n',Yticks)), ...
    'layer','top')