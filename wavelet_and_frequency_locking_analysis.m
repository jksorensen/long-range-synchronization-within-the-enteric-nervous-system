%
%     This function present results of wavelet coherence and frequency locking analysis
%     performed by the function Calculate_wavelet_and_frequency_locking_metrics
%
%     Copyright (C) 2021 Julian Sorensen
%
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.
%


function wavelet_and_frequency_locking_analysis

clc; clear all; close all;


%% Generate box plots in paper for Nicardipine, Atropine, KREBS(Rod) and TTX
load('median_Wavelet_Frequency_MAD_results.mat'); clc;
dist_vals        = [ 1 2 4 7 14 30 50 ];
dist_vals_to_use = [ 1 2 4 7 14 30 50 ];
types_to_include.include_TTX         = 1;
types_to_include.include_Nicardipine = 1;
types_to_include.include_Atropine    = 1;
types_to_include.include_Fluid       = 0;
types_to_include.include_Rod         = 1;
box_parameters = fn_initialize_box_parameters(dist_vals, dist_vals_to_use);
box_parameters.print_type          = 1;
box_parameters.x_placement_legend  = 0;
box_parameters.overlay_data        = 0;

% Frequency locking plots
fn_summary_box_plots(median_freq_MAD_activity, types_to_include, box_parameters, ...
    'Frequency Difference between Junction Potentials over Distance during to Propulsion', 'freq (Hz)');
fn_summary_box_plots(median_freq_MAD_inactivity, types_to_include, box_parameters, ...
    'Frequency Difference between Junction Potentials over Distance before to Propulsion', 'freq (Hz)')

% Wavelet coherence plots
[ no_dist, no_types ] = size(median_wcoh_MAD_abs_activity);
wcoh_increase = cell(no_dist, no_types);
for dist_idx = 1:no_dist
    for type_idx = 1:no_types
        tmp_1 = median_wcoh_MAD_abs_activity{dist_idx, type_idx};
        tmp_2 = median_wcoh_MAD_abs_inactivity{dist_idx, type_idx};
        if length(tmp_1) > 0
            wcoh_increase{dist_idx, type_idx} = tmp_1 - tmp_2;
        end
    end
end
box_parameters.type_y_placement   = 0.05;
box_parameters.x_placement_legend = 0;
fn_summary_box_plots(wcoh_increase, types_to_include, box_parameters, ...
    'Difference in Wavelet Coherence During CMCs and TTX', 'freq (Hz)')

% Generating box plots summarizing the median frequency at proximal end during propulsion event (at distal end).
box_parameters.type_y_placement = 0.05;
fn_summary_box_plots(median_cwt_1_freq_activity, types_to_include, box_parameters, ...
    'Median Frequency for Krebs, Nicardpine, Atropine and TTX', 'freq (Hz)');

% Generating box plots summarizing the time synchronization between proximal and distal colon during propulsion
% event (at oral end), as measured using phase of wavelet coherence function.
box_parameters.type_y_placement = 0.82;
types_to_include.include_TTX    = 0;
fn_summary_box_plots(median_sync_MAD_abs_activity, types_to_include,  box_parameters, ...
    'Variation in Time Difference between Junction Potentials recorded over Distance', 'sec');


%% Generate box plots in paper for Fluid
load('median_Wavelet_Frequency_MAD_results.mat'); 
dist_vals        = [ 1 2 4 7 14 30 50 ];
dist_vals_to_use = [ 1 2 4 7 14 30 ];
types_to_include.include_TTX         = 0;
types_to_include.include_Nicardipine = 0;
types_to_include.include_Atropine    = 0;
types_to_include.include_Fluid       = 1;
types_to_include.include_Rod         = 0;
box_parameters = fn_initialize_box_parameters(dist_vals, dist_vals_to_use);
box_parameters.print_type         = 1;
box_parameters.x_placement_legend = 0;
box_parameters.overlay_data       = 0;

% Generating box plots summarizing the median frequency at distal end during propulsion event (at distal end).
fn_summary_box_plots(median_cwt_1_freq_activity, types_to_include,  box_parameters, ...
    'Median frequency for fluid', 'sec');


%% Generate p-values for t-test in paper for Nicardipine, Atropine, KREBS(Rod) and TTX
load('median_Wavelet_Frequency_MAD_results.mat'); 
dist_vals        = [ 1 2 4 7 14 30 50 ];
dist_vals_to_use = [ 1 2 4 7 14 30 50 ];
types_to_include.include_TTX         = 1;
types_to_include.include_Nicardipine = 1;
types_to_include.include_Atropine    = 1;
types_to_include.include_Fluid       = 0;
types_to_include.include_Rod         = 1;

% p-values for frequency locking t-statistic
[ no_dist, no_types ] = size(median_freq_MAD_activity);
freq_error_increase   = cell(no_dist, no_types);
for dist_idx = 1:no_dist
    for type_idx = 1:no_types
        tmp_1 = median_freq_MAD_activity{dist_idx, type_idx};
        tmp_2 = median_freq_MAD_inactivity{dist_idx, type_idx};
        if length(tmp_1) > 0
            freq_error_increase{dist_idx, type_idx} = tmp_2 - tmp_1;
        end
    end
end
'Table for frequency locking (Nicardipine, Atropine, Krebs(Rod) and TTX)'
fn_data_statistic(freq_error_increase, types_to_include, dist_vals, dist_vals_to_use, no_recordings, ...
    animal_names, 0)

% p-values for wavelet coherence t-statistic
wcoh_increase = cell(no_dist, no_types);
for dist_idx = 1:no_dist
    for type_idx = 1:no_types
        tmp_1 = median_wcoh_MAD_abs_activity{dist_idx, type_idx};
        tmp_2 = median_wcoh_MAD_abs_inactivity{dist_idx, type_idx};
        if length(tmp_1) > 0
            wcoh_increase{dist_idx, type_idx} = tmp_1 - tmp_2;
        end
    end
end
'Table for wavelet coherence (Nicardipine, Atropine, Krebs(Rod) and TTX)'
fn_data_statistic(wcoh_increase, types_to_include, dist_vals, dist_vals_to_use, no_recordings, animal_names, 0)


%% Generate p-values for t-tests for Fluid in paper
load('median_Wavelet_Frequency_MAD_results.mat'); 
dist_vals        = [ 1 2 4 7 14 30 50 ];
dist_vals_to_use = [ 1 2 4 7 14 30 ];
types_to_include.include_TTX         = 0;
types_to_include.include_Nicardipine = 0;
types_to_include.include_Atropine    = 0;
types_to_include.include_Fluid       = 1;
types_to_include.include_Rod         = 0;

% p-values for frequency locking t-statistic
[ no_dist, no_types ] = size(median_freq_MAD_activity);
freq_error_increase   = cell(no_dist, no_types);
for dist_idx = 1:no_dist
    for type_idx = 1:no_types
        tmp_1 = median_freq_MAD_activity{dist_idx, type_idx};
        tmp_2 = median_freq_MAD_inactivity{dist_idx, type_idx};
        if length(tmp_1) > 0
            freq_error_increase{dist_idx, type_idx} = tmp_2 - tmp_1;
        end
    end
end
'Table for frequency locking (Fluid)'
fn_data_statistic(freq_error_increase, types_to_include, dist_vals, dist_vals_to_use, no_recordings, ...
    animal_names, 0)

% p-values for wavelet coherence t-statistic
wcoh_increase = cell(no_dist, no_types);
for dist_idx = 1:no_dist
    for type_idx = 1:no_types
        tmp_1 = median_wcoh_MAD_abs_activity{dist_idx, type_idx};
        tmp_2 = median_wcoh_MAD_abs_inactivity{dist_idx, type_idx};
        if length(tmp_1) > 0
            wcoh_increase{dist_idx, type_idx} = tmp_1 - tmp_2;
        end
    end
end
'Table for wavelet coherence (Fluid)'
fn_data_statistic(wcoh_increase, types_to_include, dist_vals, dist_vals_to_use, no_recordings, animal_names, 0)


%% Generate p-values for t-test comparing TTX to Nicardipine, Atropine and KREBS(Rod)
load('median_Wavelet_Frequency_MAD_results.mat'); 
dist_vals        = [ 1 2 4 7 14 30 50 ];
dist_vals_to_use = [ 1 2 4 7 14 30 ];

% p-values for frequency locking t-statistic
'Table for comparing frequency locking for TTX to that experienced by Nicardipine, Atropine and Krebs(Rod)'
fn_compare_TTX(median_freq_MAD_activity, dist_vals, dist_vals_to_use, no_recordings, ...
    animal_names, 0)

% p-values for wavelet coherence locking t-statistic
'Table for comparing wavelet coherence for TTX to that experienced by Nicardipine, Atropine and Krebs(Rod)'
fn_compare_TTX(median_wcoh_MAD_abs_activity, dist_vals, dist_vals_to_use, no_recordings, ...
    animal_names, 1)


%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Description:  Calculates t-stats to compare TTX wavelet coherence and frequency locking, during activity
%               at proximal colon, to that experienced by Krebs, Nicarpine and Atropine
% Input Parameters:
%     1. input_data:        2D cell containing data over all distance and types
%     2. dist_vals:         Distance values for all data
%     3. dist_vals_to_use:  Distance values to consider
%     4. no_recordings:     Number of recordings for each distance and type of recording
%     5. animal_names:      Number of animals for each distance and type of recording
%     6. mean_compare_val:  Parameter used when calculating t-test
%
% Ouput Parameters:
%     1. results_table:  Table with p-values, number of animals, recordings and bursts
%
function results_table = ...
    fn_compare_TTX(input_data, dist_vals, dist_vals_to_use, no_recordings, animal_names, mean_compare_val)

no_data_types = 4;
no_dist_vals  = sum(dist_vals_to_use > 0);
results       = cell((no_data_types*4) - 1, no_dist_vals + 2);
results(:)    = { NaN };
type_cnt = 1;
results{type_cnt,1} = 'p-values'; results{type_cnt,2} = 'Nicardpine'; type_cnt = type_cnt + 1;
results{type_cnt,1} = 'p-values'; results{type_cnt,2} = 'Atropine';   type_cnt = type_cnt + 1;
results{type_cnt,1} = 'p-values'; results{type_cnt,2} = 'Krebs(Rod)'; type_cnt = type_cnt + 1;

results{type_cnt,1} = '# animals'; results{type_cnt,2} = 'Nicardpine'; type_cnt = type_cnt + 1;
results{type_cnt,1} = '# animals'; results{type_cnt,2} = 'Atropine';   type_cnt = type_cnt + 1; 
results{type_cnt,1} = '# animals'; results{type_cnt,2} = 'Krebs(Rod)'; type_cnt = type_cnt + 1; 
results{type_cnt,1} = '# animals'; results{type_cnt,2} = 'TTX';        type_cnt = type_cnt + 1; 

results{type_cnt,1} = 'recordings'; results{type_cnt,2} = 'Nicardpine'; type_cnt = type_cnt + 1;
results{type_cnt,1} = 'recordings'; results{type_cnt,2} = 'Atropine';   type_cnt = type_cnt + 1;
results{type_cnt,1} = 'recordings'; results{type_cnt,2} = 'Krebs(Rod)'; type_cnt = type_cnt + 1; 
results{type_cnt,1} = 'recordings'; results{type_cnt,2} = 'TTX';        type_cnt = type_cnt + 1;

results{type_cnt,1} = '# bursts'; results{type_cnt,2} = 'Nicardpine'; type_cnt = type_cnt + 1; 
results{type_cnt,1} = '# bursts'; results{type_cnt,2} = 'Atropine';   type_cnt = type_cnt + 1; 
results{type_cnt,1} = '# bursts'; results{type_cnt,2} = 'Krebs(Rod)'; type_cnt = type_cnt + 1; 
results{type_cnt,1} = '# bursts'; results{type_cnt,2} = 'TTX';        type_cnt = type_cnt + 1; 

column_headers    = {};
column_headers{1} = 'Value';
column_headers{2} = 'Type';

min_no_obs = 3;
dist_cnt   = 0;
for dist_idx = 1:length(dist_vals)
    if (sum(dist_vals(dist_idx) == dist_vals_to_use) > 0)
        data_type_cnt = 0;
        column_headers{1, end+1} = [ num2str(dist_vals(dist_idx)) ' mm' ];
        
        tmp_TTX_data  = input_data{dist_idx, 6};
        if length(tmp_TTX_data) >= min_no_obs
            tmp_NIC_data  = input_data{dist_idx, 1};
            data_type_cnt = data_type_cnt + 1;
            if length(tmp_NIC_data) >= min_no_obs
                if mean_compare_val
                    t_test = fn_2_sample_t_test(tmp_TTX_data, tmp_NIC_data);
                else
                    t_test = fn_2_sample_t_test(tmp_NIC_data, tmp_TTX_data);
                end
                results{(0*no_data_types)+data_type_cnt, dist_cnt+3}  = t_test.unpooled_p_val;
                results{(1*no_data_types)+data_type_cnt-1, dist_cnt+3} = length(animal_names{dist_idx, 1});
                results{(2*no_data_types)+data_type_cnt-1, dist_cnt+3} = no_recordings(dist_idx, 1);
                results{(3*no_data_types)+data_type_cnt-1, dist_cnt+3} = length(tmp_NIC_data);                
            end
            
            tmp_ATR_data = input_data{dist_idx, 2};
            data_type_cnt  = data_type_cnt + 1;
            if length(tmp_ATR_data) >= min_no_obs
                if mean_compare_val
                    t_test = fn_2_sample_t_test(tmp_TTX_data, tmp_ATR_data);
                else
                    t_test = fn_2_sample_t_test(tmp_ATR_data, tmp_TTX_data);
                end
                results{(0*no_data_types)+data_type_cnt, dist_cnt+3}   = t_test.unpooled_p_val;
                results{(1*no_data_types)+data_type_cnt-1, dist_cnt+3} = length(animal_names{dist_idx, 2});
                results{(2*no_data_types)+data_type_cnt-1, dist_cnt+3} = no_recordings(dist_idx, 2);
                results{(3*no_data_types)+data_type_cnt-1, dist_cnt+3} = length(tmp_ATR_data);
              
            end
            
            tmp_ROD_data  = [ input_data{dist_idx, 3}, input_data{dist_idx, 4} ];
            data_type_cnt = data_type_cnt + 1;
            if length(tmp_ROD_data) >= min_no_obs
                if mean_compare_val
                    t_test = fn_2_sample_t_test(tmp_TTX_data, tmp_ROD_data);
                else
                    t_test = fn_2_sample_t_test(tmp_ROD_data, tmp_TTX_data);
                end
                results{(0*no_data_types)+data_type_cnt, dist_cnt+3}   = t_test.unpooled_p_val;
                results{(1*no_data_types)+data_type_cnt-1, dist_cnt+3} = length(animal_names{dist_idx, 3}) + ...
                    length(animal_names{dist_idx, 4});
                results{(2*no_data_types)+data_type_cnt-1, dist_cnt+3} = no_recordings(dist_idx, 3) + ...
                    no_recordings(dist_idx, 4);
                results{(3*no_data_types)+data_type_cnt-1, dist_cnt+3} = length(tmp_ROD_data);
            end
            
            data_type_cnt = data_type_cnt + 1;
            results{(1*no_data_types)+data_type_cnt-1, dist_cnt+3} = length(animal_names{dist_idx, 6});
            results{(2*no_data_types)+data_type_cnt-1, dist_cnt+3} = no_recordings(dist_idx, 6);
            results{(3*no_data_types)+data_type_cnt-1, dist_cnt+3} = length(tmp_TTX_data);
        end
        
        dist_cnt = dist_cnt + 1;
    end
end
results_table = cell2table(results, 'VariableNames', column_headers);


%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Description:  Performs t-test for input data for t-test with null_mean
%
% Input Parameters:
%     1. input_data:  Input data
%     2. null_mean:   Mean under null hypthosesis
%
% Ouput Parameters:
%     1. t_stat:  The t-statistic
%     2. t_05:    Critical point for 0.05
%     3. t_01:    Critical point for 0.01
%     4. p_val:   The p_val for t_stat
%
function [ t_stat, t_05, t_01, p_val ] = fn_t_test(input_data, null_mean)

N       = length(input_data);
mu_est  = mean(input_data);
std_est = std(input_data);

t_stat = (mu_est - null_mean) / (std_est / sqrt(N));
t_05   = tinv(0.95,N-1);
t_01   = tinv(0.99,N-1);
p_val  = 1 - tcdf(t_stat, N-1);


%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Description:  Calculates t-stats to compare wavelet coherence and frequency locking, during activity
%               at proximal colon, to that before activity
% 
% Input Parameters:
%     1. input_data:        2D cell containing data over all distance and types
%     2. types_to_include:  Types of recordings to consider in analysis
%     3. dist_vals:         Distance values for all data
%     4. dist_vals_to_use:  Distance values to consider
%     5. no_recordings:     Number of recordings for each distance and type of recording
%     6. animal_names:      Number of animals for each distance and type of recording
%     7. mean_compare_val:  Parameter used when calculating t-test
%
% Ouput Parameters:
%     1. results_table:  Table with p-values, number of animals, recordings and bursts
%
function results_table = ...
    fn_data_statistic(input_data, types_to_include, dist_vals, dist_vals_to_use, no_recordings, animal_names, mean_compare_val)

include_Nicardipine = types_to_include.include_Nicardipine;
include_Atropine    = types_to_include.include_Atropine;
include_Fluid       = types_to_include.include_Fluid;
include_Rod         = types_to_include.include_Rod;
include_TTX         = types_to_include.include_TTX;

no_data_types = include_Nicardipine + include_Atropine + include_Fluid + include_Rod + include_TTX;
no_dist_vals  = sum(dist_vals_to_use > 0);
results       = cell(no_data_types*4, no_dist_vals + 2);
results(:)    = { NaN };
type_cnt      = 1;
if include_Nicardipine; results{type_cnt,1} = 'p-values'; results{type_cnt,2} = 'Nicardpine'; type_cnt = type_cnt + 1; end
if include_Atropine;    results{type_cnt,1} = 'p-values'; results{type_cnt,2} = 'Atropine';   type_cnt = type_cnt + 1; end
if include_Rod;         results{type_cnt,1} = 'p-values'; results{type_cnt,2} = 'Krebs(Rod)'; type_cnt = type_cnt + 1; end
if include_Fluid;       results{type_cnt,1} = 'p-values'; results{type_cnt,2} = 'Fluid';      type_cnt = type_cnt + 1; end
if include_TTX;         results{type_cnt,1} = 'p-values'; results{type_cnt,2} = 'TTX';        type_cnt = type_cnt + 1; end

if include_Nicardipine; results{type_cnt,1} = '# animals'; results{type_cnt,2} = 'Nicardpine'; type_cnt = type_cnt + 1; end
if include_Atropine;    results{type_cnt,1} = '# animals'; results{type_cnt,2} = 'Atropine';   type_cnt = type_cnt + 1; end
if include_Rod;         results{type_cnt,1} = '# animals'; results{type_cnt,2} = 'Krebs(Rod)'; type_cnt = type_cnt + 1; end
if include_Fluid;       results{type_cnt,1} = '# animals'; results{type_cnt,2} = 'Fluid';      type_cnt = type_cnt + 1; end
if include_TTX;         results{type_cnt,1} = '# animals'; results{type_cnt,2} = 'TTX';        type_cnt = type_cnt + 1; end

if include_Nicardipine; results{type_cnt,1} = 'recordings'; results{type_cnt,2} = 'Nicardpine'; type_cnt = type_cnt + 1; end
if include_Atropine;    results{type_cnt,1} = 'recordings'; results{type_cnt,2} = 'Atropine';   type_cnt = type_cnt + 1; end
if include_Rod;         results{type_cnt,1} = 'recordings'; results{type_cnt,2} = 'Krebs(Rod)'; type_cnt = type_cnt + 1; end
if include_Fluid;       results{type_cnt,1} = 'recordings'; results{type_cnt,2} = 'Fluid';      type_cnt = type_cnt + 1; end
if include_TTX;         results{type_cnt,1} = 'recordings'; results{type_cnt,2} = 'TTX';        type_cnt = type_cnt + 1; end

if include_Nicardipine; results{type_cnt,1} = '# bursts'; results{type_cnt,2} = 'Nicardpine'; type_cnt = type_cnt + 1; end
if include_Atropine;    results{type_cnt,1} = '# bursts'; results{type_cnt,2} = 'Atropine';   type_cnt = type_cnt + 1; end
if include_Rod;         results{type_cnt,1} = '# bursts'; results{type_cnt,2} = 'Krebs(Rod)'; type_cnt = type_cnt + 1; end
if include_Fluid;       results{type_cnt,1} = '# bursts'; results{type_cnt,2} = 'Fluid';      type_cnt = type_cnt + 1; end
if include_TTX;         results{type_cnt,1} = '# bursts'; results{type_cnt,2} = 'TTX';        type_cnt = type_cnt + 1; end

column_headers    = {};
column_headers{1} = 'Value';
column_headers{2} = 'Type';

min_no_obs = 3;
dist_cnt   = 0;
for dist_idx = 1:length(dist_vals)
    if (sum(dist_vals(dist_idx) == dist_vals_to_use) > 0)
        data_type_cnt = 0;
        column_headers{1, end+1} = [ num2str(dist_vals(dist_idx)) ' mm' ];
        
        if include_Nicardipine
            tmp_input_data = input_data{dist_idx, 1};
            data_type_cnt  = data_type_cnt + 1;
            if length(tmp_input_data) >= min_no_obs
                [ t_stat, t_05, t_01, p_val ] = fn_t_test(tmp_input_data, mean_compare_val);
                results{(0*no_data_types)+data_type_cnt, dist_cnt+3} = (p_val);
                results{(1*no_data_types)+data_type_cnt, dist_cnt+3} = length(animal_names{dist_idx, 1});
                results{(2*no_data_types)+data_type_cnt, dist_cnt+3} = no_recordings(dist_idx, 1);
                results{(3*no_data_types)+data_type_cnt, dist_cnt+3} = length(tmp_input_data);
            end
        end
        
        if include_Atropine
            tmp_input_data = input_data{dist_idx, 2};
            data_type_cnt  = data_type_cnt + 1;
            if length(tmp_input_data) >= min_no_obs
                [ t_stat, t_05, t_01, p_val ] = fn_t_test(tmp_input_data, mean_compare_val);
                results{(0*no_data_types)+data_type_cnt, dist_cnt+3} = (p_val);
                results{(1*no_data_types)+data_type_cnt, dist_cnt+3} = length(animal_names{dist_idx, 2});
                results{(2*no_data_types)+data_type_cnt, dist_cnt+3} = no_recordings(dist_idx, 2);
                results{(3*no_data_types)+data_type_cnt, dist_cnt+3} = length(tmp_input_data);
            end
        end
        
        if include_Rod
            tmp_input_data = [ input_data{dist_idx, 3}, input_data{dist_idx, 4} ];
            data_type_cnt  = data_type_cnt + 1;
            if length(tmp_input_data) >= min_no_obs
                [ t_stat, t_05, t_01, p_val ] = fn_t_test(tmp_input_data, mean_compare_val);
                results{(0*no_data_types)+data_type_cnt, dist_cnt+3} = (p_val);
                results{(1*no_data_types)+data_type_cnt, dist_cnt+3} = length(animal_names{dist_idx, 3}) + ...
                    length(animal_names{dist_idx, 4});
                results{(2*no_data_types)+data_type_cnt, dist_cnt+3} = no_recordings(dist_idx, 3) + ...
                    no_recordings(dist_idx, 4);
                results{(3*no_data_types)+data_type_cnt, dist_cnt+3} = length(tmp_input_data);
            end
        end
        
        if include_Fluid
            tmp_input_data = input_data{dist_idx, 5};
            data_type_cnt  = data_type_cnt + 1;
            if length(tmp_input_data) >= min_no_obs
                [ t_stat, t_05, t_01, p_val ] = fn_t_test(tmp_input_data, mean_compare_val);
                results{(0*no_data_types)+data_type_cnt, dist_cnt+3} = (p_val);
                results{(1*no_data_types)+data_type_cnt, dist_cnt+3} = length(animal_names{dist_idx, 5});
                results{(2*no_data_types)+data_type_cnt, dist_cnt+3} = no_recordings(dist_idx, 5);
                results{(3*no_data_types)+data_type_cnt, dist_cnt+3} = length(tmp_input_data);
            end
        end
        
        if include_TTX
            tmp_input_data = input_data{dist_idx, 6};
            data_type_cnt  = data_type_cnt + 1;
            if length(tmp_input_data) >= min_no_obs
                [ t_stat, t_05, t_01, p_val ] = fn_t_test(tmp_input_data, mean_compare_val);
                results{(0*no_data_types)+data_type_cnt, dist_cnt+3} = (p_val);
                results{(1*no_data_types)+data_type_cnt, dist_cnt+3} = length(animal_names{dist_idx, 6});
                results{(2*no_data_types)+data_type_cnt, dist_cnt+3} = no_recordings(dist_idx, 6);
                results{(3*no_data_types)+data_type_cnt, dist_cnt+3} = length(tmp_input_data);
            end
        end
        dist_cnt = dist_cnt + 1;
    end
end
results_table = cell2table(results, 'VariableNames', column_headers);


%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Description:  Creates custom barplots to summarise input data
%
% Input Parameters:
%     1. input_data:        2D cell containing data over all distance and types
%     2. types_to_include:  Types of recordings to consider in analysis
%     3. box_parameters:    Paramters for boxplots
%     4. plot_title:        Title for box plots
%     5. y_label:           Label for y-axis
%
function fn_summary_box_plots(input_data, types_to_include, box_parameters, plot_title, y_label)

figure();

dist_vals        = box_parameters.dist_vals;
dist_vals_to_use = box_parameters.dist_vals_to_use;
no_dist_vals     = sum(dist_vals_to_use > 0);

NIC_color = box_parameters.NIC_color;
ATR_color = box_parameters.ATR_color;
FLD_color = box_parameters.FLD_color;
ROD_color = box_parameters.ROD_color;
TTX_color = box_parameters.TTX_color;

print_type            = box_parameters.print_type;
type_font_size        = box_parameters.type_font_size;
distance_font_size    = box_parameters.distance_font_size;
title_font_size       = box_parameters.title_font_size;
x_placement_legend    = box_parameters.x_placement_legend;
text_spacing          = box_parameters.text_spacing;
separation_line_style = box_parameters.separation_line_style;
y_max_multiplier      = box_parameters.y_max_multiplier;
distances_y_placement = box_parameters.distances_y_placement;
type_y_placement      = box_parameters.type_y_placement;
separation_color      = box_parameters.separation_color;

include_Nicardipine = types_to_include.include_Nicardipine;
include_Atropine    = types_to_include.include_Atropine;
include_Fluid       = types_to_include.include_Fluid;
include_Rod         = types_to_include.include_Rod;
include_TTX         = types_to_include.include_TTX;
no_data_types       = include_Nicardipine + include_Atropine + include_Fluid + include_Rod + include_TTX;

box_parameters.width_box       = box_parameters.width_box / no_data_types;
box_parameters.x_whisker_width = box_parameters.x_whisker_width / no_data_types;

start_box_pos = 0.5 / no_data_types;
min_no_obs    = 3;
min_data      = 5;
max_data      = 0;
dist_cnt      = 0;
for dist_idx = 1:length(dist_vals)
    if (sum(dist_vals(dist_idx) == dist_vals_to_use) > 0)
        bin_centre_start = dist_cnt + start_box_pos;
        data_type_cnt    = 0;
        
        if include_Nicardipine
            tmp_input_data = input_data{dist_idx, 1};
            if length(tmp_input_data) >= min_no_obs
                [ tmp_min_data, tmp_max_data ] = fn_plot_box(tmp_input_data, box_parameters, ...
                    bin_centre_start + (data_type_cnt / no_data_types), NIC_color);
                min_data      = min(min_data, tmp_min_data);
                max_data      = max(max_data,tmp_max_data);
            end
            data_type_cnt = data_type_cnt + 1;
        end
        
        if include_Atropine
            tmp_input_data = input_data{dist_idx, 2};
            if length(tmp_input_data) >= min_no_obs
                [ tmp_min_data, tmp_max_data ] = fn_plot_box(tmp_input_data, box_parameters, ...
                    bin_centre_start + (data_type_cnt / no_data_types), ATR_color);
                min_data      = min(min_data, tmp_min_data);
                max_data      = max(max_data, tmp_max_data);
            end
            data_type_cnt = data_type_cnt + 1;
        end
        
        if include_Rod
            tmp_input_data = [ input_data{dist_idx, 3}, input_data{dist_idx, 4} ];
            if length(tmp_input_data) >= min_no_obs
                [ tmp_min_data, tmp_max_data ] = fn_plot_box(tmp_input_data, box_parameters, ...
                    bin_centre_start + (data_type_cnt / no_data_types), ROD_color);
                min_data      = min(min_data, tmp_min_data);
                max_data      = max(max_data, tmp_max_data);
            end
            data_type_cnt = data_type_cnt + 1;
        end
        
        if include_Fluid
            tmp_input_data = input_data{dist_idx, 5};
            if length(tmp_input_data) >= min_no_obs
                [ tmp_min_data, tmp_max_data ] = fn_plot_box(tmp_input_data, box_parameters, ...
                    bin_centre_start + (data_type_cnt / no_data_types), FLD_color);
                min_data      = min(min_data, tmp_min_data);
                max_data      = max(max_data, tmp_max_data);
            end
            data_type_cnt = data_type_cnt + 1;
        end
        
        if include_TTX
            tmp_input_data = input_data{dist_idx, 6};
            if length(tmp_input_data) >= min_no_obs
                [ tmp_min_data, tmp_max_data ] = fn_plot_box(tmp_input_data, box_parameters, ...
                    bin_centre_start + (data_type_cnt / no_data_types), TTX_color);
                min_data      = min(min_data, tmp_min_data);
                max_data      = max(max_data, tmp_max_data);
            end
            data_type_cnt = data_type_cnt + 1;
        end
        
        dist_cnt = dist_cnt + 1;
    end
end

min_data = min(0, min_data);
y_delta  = (max_data - min_data) * y_max_multiplier;
min_data = min_data - y_delta;
max_data = max_data + y_delta;

axis tight;
for dist_idx = 1:dist_cnt
    plot([ dist_idx, dist_idx ], [ min_data, max_data ], 'Color', separation_color, 'LineStyle', separation_line_style);
end

dist_cnt = 0;
for dist_idx = 1:length(dist_vals)
    if (sum(dist_vals(dist_idx) == dist_vals_to_use) > 0)
        text(dist_cnt + 0.2, min_data - ((max_data-min_data) * 0.025), ...
            [ num2str(dist_vals(dist_idx)) 'mm' ], 'FontSize', distance_font_size);
        dist_cnt = dist_cnt + 1;
    end
end

set(gca, 'fontsize', box_parameters.fontsize_axis_no);
grid off; xlim([0 dist_cnt]);
ylim([ min_data, max_data ]);
ylabel(y_label, 'fontsize', box_parameters.fontsize_axis_label, 'fontname', box_parameters.font_name_label);
set(gca,'XTick',[]);
set(gca, 'lineWidth', box_parameters.line_width);
title(plot_title, 'FontSize', title_font_size);

bin_centre_start = start_box_pos + box_parameters.x_placement_legend;
data_type_cnt    = 0;

if include_Nicardipine
    if print_type
        h = text(bin_centre_start + (data_type_cnt / no_data_types), ...
            min_data + (max_data - min_data) * (type_y_placement - (text_spacing * 1)), ...
            'Nicardipine', 'FontSize', type_font_size, 'Color', NIC_color);
        set(h,'Rotation',90);
    end
    data_type_cnt = data_type_cnt + 1;
end
if include_Atropine
    if print_type
        h = text(bin_centre_start + (data_type_cnt / no_data_types), ...
            min_data + (max_data - min_data) * (type_y_placement - (text_spacing * 1)), ...
            'Atropine', 'FontSize', type_font_size, 'Color', ATR_color);
        set(h,'Rotation',90);
    end
    data_type_cnt = data_type_cnt + 1;
end
if include_Rod
    if print_type
        h = text(bin_centre_start + (data_type_cnt / no_data_types), ...
            min_data + (max_data - min_data) * (type_y_placement - (text_spacing * 1)), ...
            'Krebs', 'FontSize', type_font_size, 'Color', ROD_color);
        set(h,'Rotation',90);
    end
    data_type_cnt = data_type_cnt + 1;
end
if include_Fluid
    if print_type
        h = text(bin_centre_start + (data_type_cnt / no_data_types), ...
            min_data + (max_data - min_data) * (type_y_placement - (text_spacing * 1)), ...
            'Fluid', 'FontSize', type_font_size, 'Color', ATR_color);
        set(h,'Rotation',90);
    end
    data_type_cnt = data_type_cnt + 1;
end
if include_TTX
    if print_type
        h = text(bin_centre_start + (data_type_cnt / no_data_types), ...
            min_data + (max_data - min_data) * (type_y_placement - (text_spacing * 1)), ...
            'TTX', 'FontSize', type_font_size, 'Color', TTX_color);
        set(h,'Rotation',90);
    end
    data_type_cnt = data_type_cnt + 1;
end


%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Description: Plots customized boxplots
%
% Input Parameters:
%     1. input_data:       Data for box
%     2. box_parameters:   Contains most of parameters for plot
%     3. centre_position:  Position in figure for centre of box (horizontal)
%     4. box_color:        Color of box
%
% Ouput Parameters:
%     1. min_data:  Minimum value of input_data
%     2. max_data:  Maximum vlue of input_data
%
function [ min_data, max_data ] = fn_plot_box(input_data, box_parameters, centre_position, box_color)

width_box          = box_parameters.width_box;
x_whisker_width    = box_parameters.x_whisker_width;
whisker_line_style = box_parameters.whisker_line_style;
whisker_line_width = box_parameters.whisker_line_width;
median_color       = box_parameters.median_color;
whisker_color      = box_parameters.whisker_color;
outlier_color      = box_parameters.outlier_color;
outlier_size       = box_parameters.outlier_size;
median_thickness   = box_parameters.median_thickness;
overlay_data       = box_parameters.overlay_data;

sort_input_data = sort(input_data);
min_data        = sort_input_data(1);
max_data        = sort_input_data(end);
perc_val_25     = (sort_input_data(max(1, floor(end * 0.25))) + sort_input_data(max(1, ceil(end * 0.25)))) / 2;
perc_val_50     = (sort_input_data(max(1, floor(end * 0.50))) + sort_input_data(max(1, ceil(end * 0.50)))) / 2;
perc_val_75     = (sort_input_data(max(1, floor(end * 0.75))) + sort_input_data(max(1, ceil(end * 0.75)))) / 2;
IQR             = 1.5 * (perc_val_75 - perc_val_25);
bot_whisker     = max(perc_val_25 - IQR, sort_input_data(1));
top_whisker     = min(perc_val_75 + IQR, sort_input_data(end));

% Plotting box
xmin_box = centre_position - (width_box / 2);
xmax_box = centre_position + (width_box / 2);
fn_shadedplot([xmin_box xmax_box], [ perc_val_25 perc_val_25 ], [ perc_val_75 perc_val_75 ], box_color, box_color);
hold on

% Plotting median
plot([ xmin_box xmax_box ], [ perc_val_50 perc_val_50 ], 'Color', median_color, ...
    'lineWidth', median_thickness); hold on;

% Plotting whiskers
xmin_box = centre_position - (x_whisker_width / 2);
xmax_box = centre_position + (x_whisker_width / 2);
if (top_whisker > perc_val_75)
    plot([ centre_position centre_position ], [ perc_val_75 top_whisker ], 'Color', whisker_color, ...
        'LineStyle', whisker_line_style, 'lineWidth', whisker_line_width);
    plot([ xmin_box xmax_box ], [ top_whisker top_whisker ], 'Color', whisker_color', 'lineWidth', whisker_line_width);
end
if (bot_whisker < perc_val_25)
    plot([ centre_position centre_position ], [ bot_whisker perc_val_25 ], 'b', 'LineStyle', whisker_line_style, ...
        'lineWidth', whisker_line_width);
    hold on;
    plot([ xmin_box xmax_box ], [ bot_whisker bot_whisker ], 'Color', whisker_color, 'lineWidth', whisker_line_width); hold on;
end

% Plotting outliers
tmp_idx = find(sort_input_data < bot_whisker);
if length(tmp_idx) > 0
    plot(centre_position * ones(length(tmp_idx), 1), sort_input_data(tmp_idx), 'Color', outlier_color, ...
        'LineStyle', 'none', 'Marker','x', 'MarkerSize', outlier_size);
end
tmp_idx = find(sort_input_data > top_whisker);
if length(tmp_idx) > 0
    plot(centre_position * ones(length(tmp_idx), 1), sort_input_data(tmp_idx), 'Color', outlier_color, ...
        'LineStyle', 'none', 'Marker','x', 'MarkerSize', outlier_size);
end

% Overlaying all data over box plots
if overlay_data
    plot(centre_position * ones(length(sort_input_data), 1), sort_input_data, 'Color', outlier_color, ...
        'LineStyle', 'none', 'Marker','x', 'MarkerSize', outlier_size);
end

%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Description: Creates shaded areas for boxplots created by function fn_plot_box.
%
% Input Parameters:
%     1. x:         Controls x-bounds for shaded area
%     2. y1:        Controls y-bounds for shaded area
%     3. y2:        Controls y-bounds for shaded area
%     4. varargin:  Controls face color
%
function fn_shadedplot(x, y1, y2, varargin)

y  = [ y1; (y2-y1) ]';
ha = area(x, y, -20);

set(ha(1), 'FaceColor', 'none') % this makes the bottom area invisible
set(ha, 'LineStyle', 'none')

% set the line and area colors if they are specified
switch length(varargin)
    case 0
    case 1
        set(ha(2), 'FaceColor', varargin{1})
    case 2
        set(ha(2), 'FaceColor', varargin{1})
    otherwise
end


%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Description:  Initialize boxplot parameters
%
% Input Parameters:
%     1. dist_vals:  Distance values processed
%
% Output Parameters:
%     1. box_parameters:  Parameters for box plots
%
function box_parameters = fn_initialize_box_parameters(dist_vals, dist_vals_to_use)

box_parameters.dist_vals             = dist_vals;
box_parameters.dist_vals_to_use      = dist_vals_to_use;
box_parameters.width_box             = 0.6;
box_parameters.x_whisker_width       = 0.5;
box_parameters.whisker_line_style    = '--';
box_parameters.whisker_line_width    = 1;
box_parameters.separation_line_style = '-';
box_parameters.separation_color      = ones(1,3) / 256;
box_parameters.y_max_multiplier      = 0.015;
box_parameters.median_color          = ones(1,3) * 1/256;
box_parameters.whisker_color         = [ 1, 1, 1 ] / 256;
box_parameters.outlier_color         = ones(1,3) * 1 / 256;
box_parameters.outlier_size          = 10;
box_parameters.median_thickness      = 2;
box_parameters.type_font_size        = 30;
box_parameters.distance_font_size    = 24;
box_parameters.title_font_size       = 34;
box_parameters.x_placement_legend    = 0.015;
box_parameters.text_spacing          = 0.035;
box_parameters.distances_y_placement = 0.98;
box_parameters.type_y_placement      = 0.82;
box_parameters.line_width            = 1;
box_parameters.fontsize_axis_label   = 24;
box_parameters.fontsize_axis_no      = 20;
box_parameters.font_name_label       = 'arial';
box_parameters.overlay_data          = 0;
box_parameters.NIC_color             = ones(1,3) * 200 / 256;
box_parameters.ATR_color             = ones(1,3) * 170 / 256;
box_parameters.ROD_color             = ones(1,3) * 110 / 256;
box_parameters.FLD_color             = ones(1,3) * 140 / 256;
box_parameters.TTX_color             = [ 154, 154, 154 ] / 256;


%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Description: Calculates pooled and unpooled t-tests
%
% Input Parameters:
%     1. input_data_1:  Data set 1
%     2. input_data_2:  Data set 2
%
% Ouput Parameters:
%     1. t_test:  Structure t-test results
%
function t_test = fn_2_sample_t_test(input_data_1, input_data_2)

% Calculating mean and variance
n_1   = length(input_data_1);
n_2   = length(input_data_2);
mu_1  = mean(input_data_1);
mu_2  = mean(input_data_2);
std_1 = std(input_data_1);
std_2 = std(input_data_2);

% Calculating ratio of standard deviation. If it lies within [ 0.5, 2 ],
% use pooled t-test
t_test.ratio_std = std_1 / std_2;

% Calculating pooled t-test
pooled_deg_freedom   = n_1 + n_2 - 2;
s_p                  = sqrt((((n_1-1) * std_1^2) + ((n_2-1) * std_2^2)) / pooled_deg_freedom);
t_test.pooled_t_stat = (mu_2 - mu_1) / (s_p * sqrt((1/n_1) + (1/n_2)));
t_test.pooled_p_val  = 1 - tcdf(t_test.pooled_t_stat, pooled_deg_freedom);
t_test.pooled_95_pnt = tinv(0.95, pooled_deg_freedom);
t_test.pooled_99_pnt = tinv(0.99, pooled_deg_freedom);

% Calculating unpooled t-test
s_delta                = sqrt((std_1^2 / n_1) + (std_2^2 / n_2));
num_deg                = ((std_1^2 / n_1) + (std_2^2 / n_2))^2;
den_deg                = ((std_1^2 / n_1)^2 / (n_1-1)) + ((std_2^2 / n_2)^2 / (n_2-2));
unpooled_deg_freedom   = round(num_deg / den_deg);
t_test.uppooled_t_stat = (mu_2 - mu_1) / s_delta;
t_test.unpooled_p_val  = 1 - tcdf(t_test.uppooled_t_stat, unpooled_deg_freedom);
t_test.unpooled_95_pnt = tinv(0.95, unpooled_deg_freedom);
t_test.unpooled_99_pnt = tinv(0.99, unpooled_deg_freedom);