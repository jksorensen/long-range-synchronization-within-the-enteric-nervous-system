% 
%     This functions gets all subdirectories in root_dir_name. Sorts directories
%     into those that contain a subdirectory or not
% 
%     Copyright (C) 2021 Julian Sorensen  
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.
% 


%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Description: Gets all subdirectories in root_dir_name. Sorts directories
% into those that contain a subdirectory or not
%
% Input Parameters:
%     1. root_dir_name:   name of directory to search within
%
% Ouput Parameters:
%     1. dir_without_sub_dir:  Contains directories that do not contain subdirectories
%     2. dir_with_sub_dir:     Contains directories that contain subdirectories
% 
function [ dir_without_sub_dir, dir_with_sub_dir ] = find_all_subdirectories(root_dir_name)

dd          = dir(fullfile(root_dir_name, '**/*'));
isub        = [dd(:).isdir]; 
folder_list = {dd(isub).folder}';
dir_list    = {dd(isub).name}';
folder_list(ismember(dir_list,{'.','..'})) = []; 
dir_list(ismember(dir_list,{'.','..'})) = [];

empty_indicator    = zeros(length(dir_list), 1);
full_name_dir_list = dir_list;
for idx = 1:length(dir_list)
    tmp_dir_name = fullfile(folder_list{idx}, dir_list{idx});
    tmp_dd       = dir(fullfile(tmp_dir_name, '**/*'));
    tmp_isub     = [tmp_dd(:).isdir]; 
    tmp_dir_list = {tmp_dd(tmp_isub).name}';
    tmp_dir_list(ismember(tmp_dir_list,{'.','..'})) = [];
    empty_indicator(idx)    = (length(tmp_dir_list) == 0);
    full_name_dir_list{idx} = tmp_dir_name;
end
dir_without_sub_dir = full_name_dir_list(logical(empty_indicator));
dir_with_sub_dir    = full_name_dir_list(logical(empty_indicator == 0));