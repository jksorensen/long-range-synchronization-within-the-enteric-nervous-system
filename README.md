# Long Range Synchronization within the Enteric Nervous System

Contains code for analysis undertaken in "Long Range Synchronization within the Enteric Nervous System Underlies Propulsion along the Mouse Large Intestine".

Four functions are of interest to the user:
1. Calculate_wavelet_and_frequency_locking_metrics.m: The wavelet analysis (increased coherence and frequency locking) is performed by this function. The user simply needs to run the function. This function takes many hours to analyse the data. However, the results are saved so that function does not be rerun.
2. wavelet_and_frequency_locking_analysis.m: This function presents the results generated by the function "Calculate_wavelet_and_frequency_locking_metrics.m”. The user simply needs to run the function. It uses the results in the .mat file median_Wavelet_Frequency_MAD_results.mat (generated by "Calculate_wavelet_and_frequency_locking_metrics.m”)
3. analysis_L_NOARG.m: This function performs the L-Noarg analysis and presents the results of the analysis. The user simply needs to run the function.
4. example_plots.m: This function generates many of the example plots in the paper. The user simply needs to run the function. 

For the code to run
1. The data directory must be contained within the same directory as the code.
2. The signal processing and wavelet toolboxes are required.
