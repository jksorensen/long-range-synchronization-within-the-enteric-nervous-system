% 
%     This file gets type of recording and distance from file_name
% 
%     Copyright (C) 2021 Julian Sorensen  
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.
% 


%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Description: Gets type of recording and distance from file_name
%
% Input Parameters:
%     1. file_name:  Name of file to analysis
%
% Ouput Parameters:
%     1. dist_mm:           Distance in mm
%     2. dist_idx:          Index of distance
%     3. type_str:          Three letters for describing type
%     4. type_idx:          Index of type
%     5. file_name_no_num:  Name of file with file number and type removed
%
function [ dist_mm, dist_idx, type_str, type_idx, file_name_no_num ] = get_distance_type(file_name)

% Getting electrode separation
dist_mm  = -1;
dist_idx = -1;
if     (~isempty(strfind(file_name, ' 1mm')));  dist_mm = 1;  dist_idx = 1;
elseif (~isempty(strfind(file_name, ' 2mm')));  dist_mm = 2;  dist_idx = 2;
elseif (~isempty(strfind(file_name, ' 4mm')));  dist_mm = 4;  dist_idx = 3;
elseif (~isempty(strfind(file_name, ' 7mm')));  dist_mm = 7;  dist_idx = 4;
elseif (~isempty(strfind(file_name, ' 14mm'))); dist_mm = 14; dist_idx = 5;
elseif (~isempty(strfind(file_name, ' 15mm'))); dist_mm = 15; dist_idx = 5;
elseif (~isempty(strfind(file_name, ' 30mm'))); dist_mm = 30; dist_idx = 6;
elseif (~isempty(strfind(file_name, ' 40mm'))); dist_mm = 40; dist_idx = 7;
elseif (~isempty(strfind(file_name, ' 50mm'))); dist_mm = 50; dist_idx = 8;
else;
end

% Getting type of recording
type_idx         = -1;
type_str         = '';
file_name_no_num = '';

str_idx = strfind(file_name, 'NICARDIPINE');
if ~isempty(str_idx) && (type_idx < 0); type_idx = 1; type_str = 'NIC'; file_name_no_num = file_name(1:str_idx(1)-1); end
str_idx = strfind(file_name, 'nicardipine');
if ~isempty(str_idx) && (type_idx < 0); type_idx = 1; type_str = 'NIC'; file_name_no_num = file_name(1:str_idx(1)-1); end

str_idx = strfind(file_name, 'NIC & ATROP');
if ~isempty(str_idx) && (type_idx < 0); type_idx = 2; type_str = 'ATR'; file_name_no_num = file_name(1:str_idx(1)-1); end
str_idx = strfind(file_name, 'NIC and ATROP');
if ~isempty(str_idx) && (type_idx < 0); type_idx = 2; type_str = 'ATR'; file_name_no_num = file_name(1:str_idx(1)-1); end
str_idx = strfind(file_name, 'NIC');
if ~isempty(str_idx) && (type_idx < 0); type_idx = 1; type_str = 'NIC'; file_name_no_num = file_name(1:str_idx(1)-1); end

str_idx = strfind(file_name, 'ATROPINE');
if ~isempty(str_idx) && (type_idx < 0); type_idx = 2; type_str = 'ATR'; file_name_no_num = file_name(1:str_idx(1)-1); end

str_idx = strfind(file_name, 'FLUID');
if ~isempty(str_idx) && (type_idx < 0); type_idx = 5; type_str = 'FLD'; file_name_no_num = file_name(1:str_idx(1)-1); end


str_idx = strfind(file_name, 'KREBS');
if ~isempty(str_idx) && (type_idx < 0); type_idx = 3; type_str = 'KRB'; file_name_no_num = file_name(1:str_idx(1)-1); end
str_idx = strfind(file_name, 'krebs');
if ~isempty(str_idx) && (type_idx < 0); type_idx = 3; type_str = 'KRB'; file_name_no_num = file_name(1:str_idx(1)-1); end

str_idx = strfind(file_name, 'ROD');
if ~isempty(str_idx) && (type_idx < 0); type_idx = 4; type_str = 'ROD'; file_name_no_num = file_name(1:str_idx(1)-1); end

str_idx = strfind(file_name, 'TTX');
if ~isempty(str_idx) && (type_idx < 0); type_idx = 6; type_str = 'TTX'; file_name_no_num = file_name(1:str_idx(1)-1); end

str_idx = strfind(file_name, 'PUMP ON');
if ~isempty(str_idx) && (type_idx < 0); type_idx = 7; type_str = 'PUMP ON'; file_name_no_num = file_name(1:str_idx(1)-1); end

str_idx = strfind(file_name, 'PUMP OFF');
if ~isempty(str_idx) && (type_idx < 0); type_idx = 8; type_str = 'PUMP OFF'; file_name_no_num = file_name(1:str_idx(1)-1); end
