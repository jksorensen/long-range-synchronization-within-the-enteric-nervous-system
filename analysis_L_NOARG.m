% 
%     This function performs L-Noarg analysis. Files are contained in
%     '/data/L-NOARG Drug/'
% 
%     Copyright (C) 2021 Julian Sorensen  
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <https://www.gnu.org/licenses/>.
% 

function analysis_L_NOARG

close all; clc; clear all;


%% Setting parameters for filtering data and spike detection
% Parameters for filter to be applied to data
filter_parameters.filt_len = 2000;
filter_parameters.LF_Hz    = 50;
filter_parameters.HF_Hz    = 250;

% Parameters for spike detection algorithm
spike_parameters.spike_len        = 15;
spike_parameters.std_thd          = 3;
spike_parameters.min_peak_sep_sec = 0.2;
spike_parameters.search_width_sec = 0.2;


%% Finding spikes in data sets
% Get list of file names with extension .txt in directory with name dir_name
dir_name        = fullfile(pwd, '/data/L-NOARG Drug/');
list_file_names = fn_list_file_names(dir_name);

% Initializing variables that will store results
%       no_peak_ratio_KREBS:                Ratio of peaks at the distal to proximal sites in recording for KREBS            
%       proximal_peak_concentration_NOARG:  Number of spikes per second at proximal sites for NOARG
%       proximal_peak_concentration_KREBS:  Number of spikes per second at proximal sites for KREBS
%       no_peak_ratio_NOARG:                Ratio of peaks at the distal to proximal sites in recording for NOARG
%       distal_peak_concentration_NOARG:    Number of spikes per second at distal sites for NOARG
%       distal_peak_concentration_KREBS:    Number of spikes per second at distal sites for KREBS
%       file_list_krebs:                    List of KREBS file names
%       file_list_noarg:                    List of NOARG file names
no_peak_ratio_krebs               = [];
proximal_peak_concentration_noarg = [];
proximal_peak_concentration_krebs = [];
no_peak_ratio_noarg               = [];
distal_peak_concentration_noarg   = [];
distal_peak_concentration_krebs   = [];
file_list_krebs                   = {};
file_list_noarg                   = {};
for file_idx = 1:size(list_file_names, 1)
    % Loading data
    file_name = list_file_names{file_idx};
    data_mat  = fn_load_raw_data(dir_name, file_name);
    
    % Sample rate
    fs = 1 / (data_mat(2,1) - data_mat(1,1));
    
    % Boolean variable indicating if recording is Krebs
    is_krebs = (length(strfind(file_name, 'KREB')) > 0);
    
    % Applying bandpass filter to data
    filt_data_1 = fn_apply_FIR_pass_data(data_mat(:,2), fs, filter_parameters);
    filt_data_2 = fn_apply_FIR_pass_data(data_mat(:,3), fs, filter_parameters);
    
    % Finding spikes in electrical recordings
    [ peak_indices_1, matched_filter_1, matched_thd_1, big_template_1 ] = fn_find_no_peaks(filt_data_1, fs, spike_parameters);
    [ peak_indices_2, matched_filter_2, matched_thd_2, big_template_2 ] = fn_find_no_peaks(filt_data_2, fs, spike_parameters);
    if is_krebs
        no_peak_ratio_krebs(end+1)               = length(peak_indices_2) / length(peak_indices_1);
        distal_peak_concentration_krebs(end+1)   = 1000 * length(peak_indices_2) / length(matched_filter_2);
        proximal_peak_concentration_krebs(end+1) = 1000 * length(peak_indices_1) / length(matched_filter_1);
        reduced_file_name                        = file_name(1:strfind(file_name, 'KREB')-1);
        file_list_krebs{end+1}                   = reduced_file_name;
    else
        no_peak_ratio_noarg(end+1)               = length(peak_indices_2) / length(peak_indices_1);
        distal_peak_concentration_noarg(end+1)   = 1000 * length(peak_indices_2) / length(matched_filter_2);
        proximal_peak_concentration_noarg(end+1) = 1000 * length(peak_indices_1) / length(matched_filter_1);
        reduced_file_name                        = file_name(1:strfind(file_name, 'L-NOARG')-1);
        file_list_noarg{end+1}                   = reduced_file_name;
    end
end


%% Calculating t-test
t_test_proximal_conc = fn_t_test(proximal_peak_concentration_krebs, proximal_peak_concentration_noarg)
t_test_distal_conc   = fn_t_test(distal_peak_concentration_krebs, distal_peak_concentration_noarg)
t_test_peak_ratio    = fn_t_test(no_peak_ratio_krebs, no_peak_ratio_noarg)


%% Number of animals and recordings 
fprintf('\n%s%d\n', 'number recordings ainmals: ', length(unique(file_list_krebs)))
fprintf('%s%d\n', 'number recordings KREBs: ', length(distal_peak_concentration_krebs))
fprintf('%s%d\n', 'number recordings L-NOARG: ', length(distal_peak_concentration_noarg))


%% Boxplots using inbuilt Matlab boxplot functions
title_font_size  = 28;
fontsize_axis_no = 24;

% Calculating axis limits
y_min = min([ min(proximal_peak_concentration_krebs), min(proximal_peak_concentration_noarg), ...
    min(distal_peak_concentration_krebs), min(distal_peak_concentration_noarg) ]);
y_max = max([ max(proximal_peak_concentration_krebs), max(proximal_peak_concentration_noarg), ...
    max(distal_peak_concentration_krebs), max(distal_peak_concentration_noarg) ]);
y_delta = y_max - y_min;
y_min   = y_min - (0.08 * y_delta);
y_max   = y_max + (0.04 * y_delta);

figure(); subplot(1,3,1);
g1 = repmat({'Krebs'}, length(distal_peak_concentration_krebs), 1);
g2 = repmat({'L-NOARG'}, length(distal_peak_concentration_noarg), 1);
boxplot([ proximal_peak_concentration_krebs'; proximal_peak_concentration_noarg' ], [ g1; g2 ]);
set(gca, 'fontsize', fontsize_axis_no);
title('A. Peak Concentration Proximal', 'FontSize', title_font_size)
ylim([ y_min, y_max ]);

subplot(1,3,2);
g1 = repmat({'Krebs'}, length(distal_peak_concentration_krebs), 1);
g2 = repmat({'L-NOARG'}, length(distal_peak_concentration_noarg), 1);
boxplot([ distal_peak_concentration_krebs'; distal_peak_concentration_noarg' ], [ g1; g2 ]);
set(gca, 'fontsize', fontsize_axis_no);
title('B. Peak Concentration Distal', 'FontSize', title_font_size)
ylim([ y_min, y_max ]);

subplot(1,3,3); boxplot([ no_peak_ratio_krebs'; no_peak_ratio_noarg'], [g1;g2]);
set(gca, 'fontsize', fontsize_axis_no);
title('C. Peak Ratio (Distal/Proximal)', 'FontSize', title_font_size)


%% Custom boxplots - not part of Matlab
% Parameters for boxplots
box_parameters.width_box             = 0.75;
box_parameters.x_whisker_width       = 0.5;
box_parameters.whisker_line_style    = '--';
box_parameters.whisker_line_width    = 1;
box_parameters.separation_line_style = '-';
box_parameters.separation_color      = ones(1,3) / 256;
box_parameters.y_max_multiplier      = 0.015;
box_parameters.median_color          = ones(1,3) *1/256;[ 200, 0, 0 ] / 256;
box_parameters.whisker_color         = [ 1, 1, 1 ] / 256;
box_parameters.outlier_color         = ones(1,3) * 1 / 256; 
box_parameters.outlier_size          = 10;
box_parameters.median_thickness      = 2;
box_parameters.end_font_size         = 42;
box_parameters.type_font_size        = 36;
box_parameters.distance_font_size    = 24;
box_parameters.title_font_size       = 50;
box_parameters.x_placement_legend    = 0.015;
box_parameters.text_spacing          = 0.035;
box_parameters.distances_y_placement = 0.98;
box_parameters.type_y_placement      = 0.82;
box_parameters.line_width            = 1;
box_parameters.fontsize_axis_label   = 36;
box_parameters.fontsize_axis_no      = 32;
box_parameters.font_name_label       = 'arial';

figure();
KREBS_colour = ones(1,3) * 160/256;
NOARG_colour = ones(1,3) * 100/256;
fn_plot_box(proximal_peak_concentration_krebs', box_parameters, 1.5, KREBS_colour); axis tight;
fn_plot_box(proximal_peak_concentration_noarg', box_parameters, 2.5, NOARG_colour); axis tight;
fn_plot_box(distal_peak_concentration_krebs',   box_parameters, 3.5, KREBS_colour); axis tight;
fn_plot_box(distal_peak_concentration_noarg',   box_parameters, 4.5, NOARG_colour); axis tight;

set(gca, 'XTick', []);
set(gca, 'lineWidth', box_parameters.line_width);
set(gca, 'fontsize', box_parameters.fontsize_axis_no);

% Plotting titles
titles_visible = 1;
if titles_visible
    h = text(1.83, y_min - (y_max - y_min) * 0.04, ...
        'Proximal', 'FontSize', box_parameters.end_font_size, 'Color', ones(1,3) * 10 / 256);
    h = text(3.88, y_min - (y_max - y_min) * 0.04, ...
        'Distal', 'FontSize', box_parameters.end_font_size, 'Color', ones(1,3) * 10 / 256);
    h = text(1.38, y_min + (y_max - y_min) * 0.04, ...
        'KREBS', 'FontSize', box_parameters.type_font_size, 'Color', KREBS_colour);
    h = text(2.32, y_min + (y_max - y_min) * 0.04, ...
        'L-NOARG', 'FontSize', box_parameters.type_font_size, 'Color', NOARG_colour);
    h = text(3.38, y_min + (y_max - y_min) * 0.04, ...
        'KREBS', 'FontSize', box_parameters.type_font_size, 'Color', KREBS_colour);
    h = text(4.32, y_min + (y_max - y_min) * 0.04, ...
        'L-NOARG', 'FontSize', box_parameters.type_font_size, 'Color', NOARG_colour);
    title('Average Number Spikes Per Second', 'FontSize', box_parameters.title_font_size)
end
    
% Setting axis limits
plot([3,3], [ y_min, y_max ], 'k');
ylim([ y_min, y_max ]);
xlim([ 1, 5 ]);


%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Description: Plots customized boxplots
%
% Input Parameters:
%     1. input_data:       Data for box
%     2. box_parameters:   Contains most of parameters for plot
%     3. centre_position:  Position in figure for centre of box (horizontal)
%     4. box_color:        Color of box
%
% Ouput Parameters:
%     1. min_data:  Minimum value of input_data
%     2. max_data:  Maximum vlue of input_data
% 
function [ min_data, max_data ] = fn_plot_box(input_data, box_parameters, centre_position, box_color)

width_box          = box_parameters.width_box;
x_whisker_width    = box_parameters.x_whisker_width;
whisker_line_style = box_parameters.whisker_line_style;
whisker_line_width = box_parameters.whisker_line_width;
median_color       = box_parameters.median_color;
whisker_color      = box_parameters.whisker_color;
outlier_color      = box_parameters.outlier_color;
outlier_size       = box_parameters.outlier_size;
median_thickness   = box_parameters.median_thickness;

sort_input_data = sort(input_data);
min_data        = sort_input_data(1);
max_data        = sort_input_data(end);
perc_val_25     = (sort_input_data(max(1, floor(end * 0.25))) + sort_input_data(max(1, ceil(end * 0.25)))) / 2;
perc_val_50     = (sort_input_data(max(1, floor(end * 0.50))) + sort_input_data(max(1, ceil(end * 0.50)))) / 2;
perc_val_75     = (sort_input_data(max(1, floor(end * 0.75))) + sort_input_data(max(1, ceil(end * 0.75)))) / 2;
IQR             = 1.5 * (perc_val_75 - perc_val_25);
bot_whisker     = max(perc_val_25 - IQR, sort_input_data(1));
top_whisker     = min(perc_val_75 + IQR, sort_input_data(end));

% Plotting box
xmin_box = centre_position - (width_box / 2);
xmax_box = centre_position + (width_box / 2);
fn_shadedplot([xmin_box xmax_box], [ perc_val_25 perc_val_25 ], [ perc_val_75 perc_val_75 ], box_color, box_color);
hold on

% Plotting median
plot([ xmin_box xmax_box ], [ perc_val_50 perc_val_50 ], 'Color', median_color, ...
    'lineWidth', median_thickness); hold on;

% Plotting whiskers
xmin_box = centre_position - (x_whisker_width / 2);
xmax_box = centre_position + (x_whisker_width / 2);
if (top_whisker > perc_val_75)
    plot([ centre_position centre_position ], [ perc_val_75 top_whisker ], 'Color', whisker_color, ...
        'LineStyle', whisker_line_style, 'lineWidth', whisker_line_width);
    plot([ xmin_box xmax_box ], [ top_whisker top_whisker ], 'Color', whisker_color', 'lineWidth', whisker_line_width);
end
if (bot_whisker < perc_val_25)
    plot([ centre_position centre_position ], [ bot_whisker perc_val_25 ], 'b', 'LineStyle', whisker_line_style, ...
        'lineWidth', whisker_line_width);
    hold on;
    plot([ xmin_box xmax_box ], [ bot_whisker bot_whisker ], 'Color', whisker_color, 'lineWidth', whisker_line_width); hold on;
end

% Plotting outliers
tmp_idx = find(sort_input_data < bot_whisker);
if length(tmp_idx) > 0
    plot(centre_position * ones(length(tmp_idx), 1), sort_input_data(tmp_idx), 'Color', outlier_color, ...
        'LineStyle', 'none', 'Marker','x', 'MarkerSize', outlier_size);
end
tmp_idx = find(sort_input_data > top_whisker);
if length(tmp_idx) > 0
    plot(centre_position * ones(length(tmp_idx), 1), sort_input_data(tmp_idx), 'Color', outlier_color, ...
        'LineStyle', 'none', 'Marker','x', 'MarkerSize', outlier_size);
end


%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Description: Creates shaded areas for boxplots created by function fn_plot_box.
%
% Input Parameters:
%     1. x:         Controls x-bounds for shaded area
%     2. y1:        Controls y-bounds for shaded area
%     3. y2:        Controls y-bounds for shaded area
%     4. varargin:  Controls face color
% 
function fn_shadedplot(x, y1, y2, varargin)

y  = [ y1; (y2-y1) ]';
ha = area(x, y, -20);

set(ha(1), 'FaceColor', 'none') % this makes the bottom area invisible
set(ha, 'LineStyle', 'none')

% set the line and area colors if they are specified
switch length(varargin)
    case 0
    case 1
        set(ha(2), 'FaceColor', varargin{1})
    case 2
        set(ha(2), 'FaceColor', varargin{1})
    otherwise
end


%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Description: Finds spikes within input time series. It forms a spike
%              template from the largest peak within time series and 
%              then correlates this template against data.
%
% Input Parameters:
%     1. input_data:        Time series containing spikes
%     2. fs:                Sample rate
%     3. spike_parameters:  Parameters for finding spikes
%
% Ouput Parameters:
%     1. peak_indices:    Indices of detected spikes
%     2. matched_filter:  The correlation of the template against input_data 
%     3. match_thd:       Used to determine threshold matched_filter.
%     4. big_template:    Template of spike using biggest peak
% 
function [ peak_indices, matched_filter, matched_thd, big_template ] = ...
    fn_find_no_peaks(input_data, fs, spike_parameters)

% Extracting parameters for finding spikes
%       spike_len:          Length of template for spikes.
%       std_thd:            The threshold for used to determine if peak is significant
%       min_peak_sep_sec:   Minimum spacing (in seconds) between peaks
%       search_width_pnts:  Used to determine the width (in seconds) to
%                           search for spike for points above threshold
spike_len        = spike_parameters.spike_len;
std_thd          = spike_parameters.std_thd;
min_peak_sep_sec = spike_parameters.min_peak_sep_sec;
search_width_sec = spike_parameters.search_width_sec;

% Converting min_peak_sep_sec and search_width_sec to number of points
min_peak_sep_pnts = round(min_peak_sep_sec * fs);
search_width_pnts = round(search_width_sec * fs);

% Generating spike template using largest peak
[ ~, max_idx ] = max(input_data);
big_template   = input_data(max_idx(1)-spike_len:max_idx(1)+spike_len);

% Correlating spike template against data
matched_filter = xcorr(input_data, big_template);
matched_filter = matched_filter(length(input_data):end);

% Calculate threshold
matched_thd = std_thd * std(matched_filter);

% Finding peaks above threshold in matched_filters
above_theshold = (matched_filter > matched_thd);
current_idx    = 1;
stop_cond      = 1;
peak_indices   = [];
while stop_cond == 1
    tmp_idx = find(above_theshold, 1, 'first');
    if length(tmp_idx) > 0
        [ ~, max_idx ]      = max(matched_filter(tmp_idx(1):min(tmp_idx(1)+search_width_pnts, end)));
        peak_indices(end+1) = max_idx(1) + tmp_idx(1) - 1;
        current_idx         = peak_indices(end) + min_peak_sep_pnts;
        above_theshold(max_idx(1):min(current_idx, end)) = 0;
    else
        stop_cond = 0;
    end 
end


%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Description: Applies FIR filter to data
%
% Input Parameters:
%     1. input_data:   Data to be filtered
%     2. fs:           Sample rate
%     3. filter_parameters:  Filter parameters
%
% Ouput Parameters:
%     1. filt_data:      Filtered data
% 
function [ filt_data ] = fn_apply_FIR_pass_data(input_data, fs, filter_parameters)

filt_len = 2000;
LF_Hz    = filter_parameters.LF_Hz;
HF_Hz    = filter_parameters.HF_Hz;

filt_coef = fir1(filt_len, [ LF_Hz, HF_Hz ] / fs);
filt_data = filter(filt_coef, 1, input_data);
filt_data = filt_data(filt_len:end);


%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Description: Loads raw data from .txt file
%
% Input Parameters:
%     1. dir_name:   Name of directory
%     2. file_name:  Name of file
%
% Ouput Parameters:
%     1. data_mat:   Matrix containing data. First column contains time indices, other
%                    columns contain electrical recordings
% 
function data_mat = fn_load_raw_data(dir_name, file_name)

% Reading string from file
file_path = [ dir_name, file_name ];
full_str  = fileread(file_path);
tmp_str   = full_str(1:400);

% Getting the number of columns
range_idx  = strfind(full_str, 'Range=');
unit_idx   = strfind(full_str, 'Unitname=');
no_columns = length(strfind(full_str(range_idx(1)+length('Range='):unit_idx-1), 'V')) + 1;

% Data can be in two formats
if (isempty(strfind(tmp_str, 'Interval=')) || isempty(strfind(tmp_str, 'ms')) || isempty(strfind(tmp_str, 'BottomValue='))) == 0
    SR_start = strfind(tmp_str, 'Interval=') + length('Interval=');
    SR_end   = strfind(tmp_str, 'ms') - 1;
    SR       = str2num(tmp_str(SR_start:SR_end));
    
    tmp_idx  = strfind(tmp_str, 'BottomValue=') + length('BottomValue=');
    CR_idx   = strfind(tmp_str(tmp_idx:tmp_idx+100), sprintf('\r'));
    data_str = full_str(CR_idx(1) + tmp_idx: end);
    data_vec = sscanf(data_str, '%f');
    num_rows = length(data_vec) / no_columns;
    data_mat = reshape(data_vec, no_columns, num_rows)';
else
    data_vec = sscanf(full_str, '%f');
    num_rows = length(data_vec) / no_columns;
    data_mat = reshape(data_vec, no_columns, num_rows)';
end

if no_columns == 5
    data_mat = data_mat(:, [ 1 2 4 ]);
end


%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Description: Gets list of file names with extension .txt in directory
%
% Input Parameters:
%     1. dir_name:  Name of directory
%
% Ouput Parameters:
%     1. list_file_names:  List containing filenames with extension .txt
% 
function list_file_names = fn_list_file_names(dir_name)

file_contents   = dir(dir_name);
list_file_names = {};
cnt             = 0;
for idx = 1:size(file_contents,1)
    if length(strfind(file_contents(idx).name, '.txt')) == 1
        cnt                     = cnt + 1;
        list_file_names{cnt, 1} = file_contents(idx).name;
    end
end


%% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Description: Calculates pools and unpooled t-tests 
%
% Input Parameters:
%     1. input_data_1:  Data set 1
%     2. input_data_2:  Data set 2
%
% Ouput Parameters:
%     1. t_test:  Structure t-test results
% 
function t_test = fn_t_test(input_data_1, input_data_2)

% Calculating mean and variance
n_1   = length(input_data_1);
n_2   = length(input_data_2);
mu_1  = mean(input_data_1);
mu_2  = mean(input_data_2);
std_1 = std(input_data_1);
std_2 = std(input_data_2);

% Calculating ratio of standard deviation. If it lies within [ 0.5, 2 ],
% use pooled t-test
t_test.ratio_std = std_1 / std_2;

% Calculating pooled t-test
pooled_deg_freedom   = n_1 + n_2 - 2;
s_p                  = sqrt((((n_1-1) * std_1^2) + ((n_2-1) * std_2^2)) / pooled_deg_freedom);
t_test.pooled_t_stat = (mu_2 - mu_1) / (s_p * sqrt((1/n_1) + (1/n_2)));
t_test.pooled_p_val  = 1 - tcdf(t_test.pooled_t_stat, pooled_deg_freedom);
t_test.pooled_95_pnt = tinv(0.95, pooled_deg_freedom);
t_test.pooled_99_pnt = tinv(0.99, pooled_deg_freedom);

% Calculating unpooled t-test
s_delta                = sqrt((std_1^2 / n_1) + (std_2^2 / n_2));
num_deg                = ((std_1^2 / n_1) + (std_2^2 / n_2))^2;
den_deg                = ((std_1^2 / n_1)^2 / (n_1-1)) + ((std_2^2 / n_2)^2 / (n_2-2));
unpooled_deg_freedom   = round(num_deg / den_deg);
t_test.uppooled_t_stat = (mu_2 - mu_1) / s_delta;
t_test.unpooled_p_val  = 1 - tcdf(t_test.uppooled_t_stat, unpooled_deg_freedom);
t_test.unpooled_95_pnt = tinv(0.95, unpooled_deg_freedom);
t_test.unpooled_99_pnt = tinv(0.99, unpooled_deg_freedom);